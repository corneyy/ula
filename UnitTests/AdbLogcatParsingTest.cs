﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace UnitTests
{
    [TestClass]
    public class AdbLogcatParsingTest : TestBase
    {
        List<string> lines;
        List<Ula.Adb.LogRecord> records;

        static void load(string fileName, Ula.Adb.Parser.RecordReady cb)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(System.IO.File.OpenRead(fileName)))
            {
                Ula.Adb.Parser.Parse(() => { return (reader.EndOfStream ? null : reader.ReadLine()); }, cb);
            }
        }

        static void loadO2O(string fileName, Ula.Adb.Parser.RecordReady cb)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(System.IO.File.OpenRead(fileName)))
            {
                while (!reader.EndOfStream)
                {
                    Ula.Adb.Parser.ParseOneToOne(reader.ReadLine(), cb);
                }
            }
        }


        [TestInitialize]
        public void Init()
        {
            lines = new List<string>();
            records = new List<Ula.Adb.LogRecord>();
        }

        [TestMethod]
        public void TestBriefFormatO2O()
        {
            loadO2O(basePath+"/test-up-logcat.txt", (r) => lines.Add(r.content));

            Assert.AreEqual(2779, lines.Count); // type test-up-logcat.txt | find "/Unity   " /c
            Assert.AreEqual("LD: (-7.5, -5.0, -10.0)", lines[49-1]); // type test-up-logcat.txt | find "/Unity   " | find "LD:" /n
            Assert.AreEqual("Android Unity internal profiler stats:", lines[2779 - 4]);
        }

        [TestMethod]
        public void TestTimeFormatO2O()
        {
            loadO2O(basePath + "/test-up-logcat-time.txt", (r) => lines.Add(r.content));

            Assert.AreEqual(2480, lines.Count); // type test-up-logcat-time.txt | find "/Unity   " /c
            Assert.AreEqual("Cannot change GameObject hierarchy while activating or deactivating the parent.",
                lines[1901 - 1]); // type test-up-logcat-time.txt | find "/Unity   " | find "Cannot change" /n
            Assert.AreEqual("Android Unity internal profiler stats:", lines[2480 - 12]);
        }

        [TestMethod]
        public void TestBriefFormat()
        {
            load(basePath + "/test-trace-up-logcat.txt", (r) => lines.Add(r.content));

            Assert.AreEqual(26, lines.Count); // type test-trace-up-logcat.txt | find "/Unity   " /c 
            Assert.AreEqual("LD: (-7.5, -5.0, -10.0)", lines[9 - 1]); // type test-trace-up-logcat.txt | find "/Unity   " | find "LD:" /n
            Assert.AreEqual("WS: (15.0, 10.0, 0.0)", lines[32 - 21 -1]);
            Assert.AreEqual("Android Unity internal profiler stats:", lines[lines.Count - 11 - 1]); 
        }

        [TestMethod]
        public void TestTimeFormat()
        {
            load(basePath + "/test-trace-up-logcat-time.txt", (r) => records.Add(r));

            Assert.AreEqual(23, records.Count);
            Assert.AreEqual(new DateTime(DateTime.Now.Year,02,16,15,25,42,513), records[6 - 1].datetime);
            Assert.AreEqual("NPB: worker2 (-6.8, -4.8, 0.0) moveTo (0.4, 2.7, 0.0)", records[6 - 1].content);
            Assert.AreEqual("0->3", records[8].content);
            Assert.AreEqual(new DateTime(DateTime.Now.Year, 02, 16, 15, 25, 43, 123), records[records.Count - 11 - 1].datetime);
            Assert.AreEqual("Android Unity internal profiler stats:", records[records.Count - 11 - 1].content); 
        }
    }
}
