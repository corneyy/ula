﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Ula.Features.Synchronization;

namespace UnitTests
{
    [TestClass]
    public class SynchronizatorTest
    {
        const int maxAge = 10;
        const int step = 5;

        Ula.Features.Synchronization.UnlimitedSynchronizator syncer;
        DateTime newest;

        [TestInitialize]
        public void Init()
        {
            syncer = new UnlimitedSynchronizator(maxAge);

            DateTime start = DateTime.Now;
            for (int s = 10; s < 55; s += step)
            {
                newest = start + new TimeSpan(0, 0, s);
                syncer.NextLineIdx(newest);
            }
        }

        [TestMethod]
        public void TestMaxAge()
        {
            int sampleIdx = syncer.NextSampleIdx(newest - new TimeSpan(0, 0, 0, maxAge, 1));

            Assert.AreEqual(Synchronizator.invalidList, syncer.LineIdx(sampleIdx));
        }

        [TestMethod]
        public void TestMissSec()
        {
            int sampleIdx = syncer.NextSampleIdx(newest - new TimeSpan(0, 0, 1));

            Assert.AreEqual(Synchronizator.invalidList, syncer.LineIdx(sampleIdx));
        }

        [TestMethod]
        public void TestMissMSec()
        {
            int sampleIdx = syncer.NextSampleIdx(newest - new TimeSpan(0, 0, 0, 0, 1));

            Assert.AreEqual(Synchronizator.invalidList, syncer.LineIdx(sampleIdx));
        }

        [TestMethod]
        public void TestEqOldestTime()
        {
            int sampleIdx = syncer.NextSampleIdx(newest - new TimeSpan(0, 0, maxAge));

            IList<int> lines = syncer.LineIdx(sampleIdx);
            Assert.AreEqual(1, lines.Count);
            Assert.AreEqual(6, lines[0]);
            Assert.AreEqual(sampleIdx, syncer.SampleIdx(6));
        }

        [TestMethod]
        public void TestEqNewestTime()
        {
            int sampleIdx = syncer.NextSampleIdx(newest );

            IList<int> lines = syncer.LineIdx(sampleIdx);
            Assert.AreEqual(1, lines.Count);
            Assert.AreEqual(8, lines[0]);
            Assert.AreEqual(sampleIdx, syncer.SampleIdx(8));
        }

        [TestMethod]
        public void TestEqOldestNewestTime()
        {
            int sampleIdx0 = syncer.NextSampleIdx(newest - new TimeSpan(0, 0, maxAge));
            int sampleIdx1 = syncer.NextSampleIdx(newest);

            IList<int> lines0 = syncer.LineIdx(sampleIdx0);
            Assert.AreEqual(1, lines0.Count);
            Assert.AreEqual(6, lines0[0]);
            Assert.AreEqual(sampleIdx0, syncer.SampleIdx(6));

            IList<int> lines1 = syncer.LineIdx(sampleIdx1);
            Assert.AreEqual(1, lines1.Count);
            Assert.AreEqual(8, lines1[0]);
            Assert.AreEqual(sampleIdx1, syncer.SampleIdx(8));
        }

        [TestMethod]
        public void TestSync()
        {
            int sampleIdx = syncer.NextSampleIdx(newest - new TimeSpan(0, 0, step-1));

            IList<int> lines = syncer.LineIdx(sampleIdx);
            Assert.AreEqual(1, lines.Count);
            Assert.AreEqual(7, lines[0]);
        }

        [TestMethod]
        public void TestMultiSync()
        {
            syncer.NextLineIdx(newest + new TimeSpan(0, 0, 0, 1, 1));
            syncer.NextLineIdx(newest + new TimeSpan(0, 0, 0, 1, 10));
            syncer.NextLineIdx(newest + new TimeSpan(0, 0, 0, 1, 100));

            int sampleIdx = syncer.NextSampleIdx(newest + new TimeSpan(0, 0, 2));

            IList<int> lines = syncer.LineIdx(sampleIdx);
            Assert.AreEqual(3, lines.Count);
            Assert.AreEqual( 9, lines[0]);
            Assert.AreEqual(10, lines[1]);
            Assert.AreEqual(11, lines[2]);
        }
    }
}
