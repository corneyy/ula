﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;

namespace UnitTests
{
    [TestClass]
    public class UnityIPFilterTest: TestBase
    {
        List<string> records;
        Ula.Unity.IPSampleFilter filter;

        static void load(string fileName, Ula.Adb.Parser.RecordReady cb)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(System.IO.File.OpenRead(fileName)))
            {
                while (!reader.EndOfStream)
                {
                    string[] split=reader.ReadLine().Split(new char[] { ':' }, 2);
                    if(2==split.Length) cb(new Ula.Adb.LogRecord(split[1].Trim()));
                }
            }
        }

        [TestInitialize]
        public void Init()
        {
            records = new List<string>();
            filter = new Ula.Unity.IPSampleFilter();
        }


        [TestMethod]
        public void TestFilter()
        {
            load(basePath + "/test-profiler-up-logcat.txt", (r) =>{ 
                Ula.Adb.LogRecord filtered=filter.Filter(r);
                if (null != filtered && null != filtered.content) records.Add(filtered.content);
            });

            Assert.AreEqual(7, records.Count);
            Assert.AreEqual("PlayerConnection already initialized - listening to [0.0.0.0:55203]", records[records.Count-1]);
        }
        
        [TestMethod]
        public void TestFilterSamplesCallBack()
        {
            int counter = 0;
            filter.SampleReadyEvent += (s,dt) => ++counter;

            load(basePath + "/test-profiler-up-logcat.txt", (s) => filter.Filter(s));

            Assert.AreEqual(25, counter);
        }

        [TestMethod]
        public void TestFilterParser()
        {
            string textSample =
@"Android Unity internal profiler stats:
cpu-player>    min: 10.1   max: 36.4   avg: 16.5 
cpu-ogles-drv> min:  0.3   max:  1.6   avg:  0.5
cpu-present>   min: -22.3   max:  0.3   avg: -0.3
frametime>     min: 10.5   max: 22.4   avg: 16.7
draw-call #>   min:  13    max:  15    avg:  14     | batched:    80
tris #>        min:   216  max:   218  avg:   217   | batched:   160
verts #>       min:   452  max:   456  avg:   454   | batched:   320
player-detail> physx:  0.4 animation:  1.3 culling  0.1 skinning:  0.0 batching:  0.5 render: 11.2 fixed-update-count: 0 .. 2
mono-scripts>  update:  0.5   fixedUpdate:  0.1 coroutines:  0.0 
mono-memory>   used heap: 393216 allocated heap: 479232  max number of collections: 0 collection total duration:  0.0
----------------------------------------
";
            Ula.Unity.IPSample sample=new Ula.Unity.IPSample();
            filter.SampleReadyEvent += (s, dt) => sample=s;
            string error="";
            filter.UnknownParameterEvent += (s) => error=s;

            using (StringReader reader = new StringReader(textSample))
            {
                string line;
                while (null!=(line=reader.ReadLine())) filter.Filter(new Ula.Adb.LogRecord(line));
            }

            Assert.AreEqual("", error);
            Assert.AreEqual(10.1, sample.cpu_player.min, 0.0001);
            Assert.AreEqual(36.4, sample.cpu_player.max, 0.0001);
            Assert.AreEqual(16.5, sample.cpu_player.avg, 0.0001);

            Assert.AreEqual(-22.3, sample.cpu_present.min, 0.0001);
            Assert.AreEqual(  0.3, sample.cpu_present.max, 0.0001);
            Assert.AreEqual( -0.3, sample.cpu_present.avg, 0.0001);

            Assert.AreEqual(13, sample.draw_call.min);
            Assert.AreEqual(15, sample.draw_call.max);
            Assert.AreEqual(14, sample.draw_call.avg);
            Assert.AreEqual(80, sample.draw_call.batched);

            Assert.AreEqual(452, sample.verts.min);
            Assert.AreEqual(456, sample.verts.max);
            Assert.AreEqual(454, sample.verts.avg);
            Assert.AreEqual(320, sample.verts.batched);

            Assert.AreEqual( 0.4, sample.player_detail.physx, 0.0001);
            Assert.AreEqual( 1.3, sample.player_detail.animation, 0.0001);
            Assert.AreEqual( 0.1, sample.player_detail.culling, 0.0001);
            Assert.AreEqual( 0.0, sample.player_detail.skinning, 0.0001);
            Assert.AreEqual( 0.5, sample.player_detail.batching, 0.0001);
            Assert.AreEqual(11.2, sample.player_detail.render, 0.0001);
            Assert.AreEqual(   0, sample.player_detail.fixed_update_count_min);
            Assert.AreEqual(   2, sample.player_detail.fixed_update_count_max);

            Assert.AreEqual(0.5, sample.mono_scripts.update, 0.0001);
            Assert.AreEqual(0.1, sample.mono_scripts.fixedUpdate, 0.0001);
            Assert.AreEqual(0.0, sample.mono_scripts.coroutines, 0.0001);

            Assert.AreEqual(393216, sample.mono_memory.used_heap);
            Assert.AreEqual(479232, sample.mono_memory.allocated_heap);
            Assert.AreEqual(     0, sample.mono_memory.max_number_of_collections);
            Assert.AreEqual(   0.0, sample.mono_memory.collection_total_duration,0.0001);
        }
    }
}
