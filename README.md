Unity android log & internal profiler samples viewer and analyzer.

Application features:

* Shows plots using internal Unity profiler data
* Displays statistics' of frame rate and garbage collector
* Shows statistics plots for fast and easy problem zones detection
* Displays Unitys' log entries without stack trace
* Synchronization between plots and log entries

Log entries can be fetched:

* using direct connection to mobile device via adb
* using file with adb logcat output

Screenshoots:

![](http://farbow.ru/ula/ula_shot03_thumb.png)
![](http://farbow.ru/ula/ula_shot04_thumb.png)

![](http://farbow.ru/ula/ula_shot01_thumb.png)
![](http://farbow.ru/ula/ula_shot02_thumb.png)

Detailed description: http://farbow.ru/ula/en

Binaries:

* ClickOnce offline install: http://farbow.ru/ula/arch/ula095.zip
* Internet Explorer ClickOnce Web install: http://farbow.ru/ula/setup/Ula.application
* ClickOnce Web install: http://farbow.ru/ula/setup/setup.exe