﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;

namespace Ula.Features.LogPlots
{
    public class HistogramViewModel: Utils.NotifyPropertyChanged
    {
        PlotModel plotModel;
        CategoryAxis axis;
        ColumnSeries series;

        public HistogramViewModel(string title)
        {
            PlotsModel = new OxyPlot.PlotModel()
            {
                PlotAreaBorderThickness = new OxyPlot.OxyThickness(0),
                PlotMargins = new OxyPlot.OxyThickness(Double.NaN),
                IsLegendVisible = false,
                TitleFontSize = 12,
                TitleFontWeight = FontWeights.Normal,
                Title = title
            };

            OxyPlot.PlotModel model = PlotsModel;

            model.Axes.Add(new OxyPlot.Axes.LogarithmicAxis()
            {
                Position = OxyPlot.Axes.AxisPosition.Left,
                IsZoomEnabled = false,
                IsPanEnabled = false,
                AxislineStyle = OxyPlot.LineStyle.Solid,
                AbsoluteMinimum = 0,
                Minimum = 0.1,
                StringFormat = "N0"
            });

            model.Axes.Add(axis = new OxyPlot.Axes.CategoryAxis()
            {
                IsZoomEnabled = false,
                IsPanEnabled = false,
                Position = OxyPlot.Axes.AxisPosition.Bottom,
            });

            model.Series.Add(series = new OxyPlot.Series.ColumnSeries()
            {
                BaseValue = 0.1
            });
        }

        public void New(Action<HistogramViewModel> adder)
        {
            axis.Labels.Clear();
            series.Items.Clear();

            adder(this);

            plotModel.InvalidatePlot(true);
        }

        public void Add(string label, double value)
        {
            axis.Labels.Add(label);
            series.Items.Add(new OxyPlot.Series.ColumnItem(value < 0.1 ? 0.1 : value));
        }

        public void Clear()
        {
            axis.Labels.Clear();
            series.Items.Clear();

            plotModel.InvalidatePlot(true);
        }

        public OxyPlot.PlotModel PlotsModel
        {
            get { return plotModel; }
            set { plotModel = value; OnPropertyChanged("PlotsModel"); }
        }
    }
}
