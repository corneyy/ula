﻿using System;
using System.Collections.Generic;
using OxyPlot;
using OxyPlot.Axes;
using Ula.Plots;
using OxyPlot.Annotations;
using System.Linq;

namespace Ula.Features.LogPlots
{
    public abstract class PlotsViewModel : Utils.NotifyPropertyChanged, Features.ISingleSelectable
    {
        readonly double resolution = 2; // one plot point to two screen point 
        PlotModel plotModel;
        OP.IPController controller=new OP.IPController();
        LinearAxis dateAxis;
        double axisScreenLength = 0;
        bool isShowNewest = true;
        int limit = 1000;
        bool isLimited = false;
        double scrollMin;
        double scrollMax;
        double scrollPos;
        double scrollViewport;
        double scrollLarge;
        double scrollSmall;
        int idxMin;
        int idxMax;

        LineAnnotation externalSelectAnnotation;
        LineAnnotation selectAnnotation;
        RectangleAnnotation rangeAnnotation;

        protected PVAllocator allocator = new PVAllocator(1000);

        List<IPlotIPSampleSeries> sampleParameters=null;
        Dictionary<string, IPlotValueSeries> valueParameters = null;

        readonly static string annotationsKey = "anns";

        List<LinearAxis> axis;

        public PlotsViewModel(List<LinearAxis> axis)
        {
            this.axis = axis;
            axis.Add(new LinearAxis()
            {
                Key = annotationsKey,
                Position = AxisPosition.Left,
                AbsoluteMinimum = 0,
                Minimum = 0,
                IsAxisVisible = false,
                IsZoomEnabled = false,
                IsPanEnabled = false,
            });

            InitChild();

            InitPlots();

            plotModel.Updated += (a, b) =>
            {
                if (0 == axisScreenLength)
                {
                    RecalcView(false);
                    if (SamplesNum() > 1) RealScrollTo(dateAxis.AbsoluteMaximum, false);
                }
            };
        }

        protected abstract void InitChild();

        protected void Parameters(List<IPlotIPSampleSeries> sampleParameters, Dictionary<string, IPlotValueSeries> valueParameters)
        {
            this.sampleParameters = sampleParameters;
            this.valueParameters = valueParameters;
        }

        public void NewSampleOnline(int idx, Unity.IPSample sample)
        {
            NewSample(idx, sample);

            if (0 == axisScreenLength) return;

            ProcessOnline();
        }

        public void NewSampleOffline(int idx, Unity.IPSample sample)
        {
            NewSample(idx, sample);
        }

        public void Offlined()
        {
            if (0 == axisScreenLength) return;

            SetPlotViewport();

            RealScrollTo(dateAxis.AbsoluteMaximum, false);

            plotModel.InvalidatePlot(true);
        }

        void NewSample(int idx, Unity.IPSample sample)
        {
            idxMax = idx;
            AdjustIdxMin();

            sampleParameters.ForEach((p) => p.Add(idx, sample));
        }

        public void Clear()
        {
            idxMin = 0;
            idxMax = 0;
            InternalScrollPos = 0;
            sampleParameters.ForEach((p) => p.Clear());
            valueParameters.ToList().ForEach((pair) => pair.Value.Clear());
            
            Unselect();

            plotModel.InvalidatePlot(true);
        }

        void InitPlots()
        {
            PlotsModel = new PlotModel()
            {
                PlotAreaBorderThickness = new OxyThickness(0),
                PlotMargins = new OxyThickness(Double.NaN),
                LegendBackground = OxyColor.FromArgb(100, 255, 255, 255),
                LegendPlacement = LegendPlacement.Outside,
                IsLegendVisible =  false
            };

            PlotModel model = PlotsModel;

            dateAxis = new LinearAxis()
            {
                Position = AxisPosition.Bottom,
                FormatAsFractions = false,
#if DEBUG
                IsAxisVisible = true,
#else
                IsAxisVisible = false,
#endif
                IsZoomEnabled = false,
                IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid,
                MaximumPadding = 0.001,
                MinimumPadding = 0.001,
                AbsoluteMinimum = 0,
                Minimum = 0,
                MajorStep = 100
            };
            model.Axes.Add(dateAxis);

            externalSelectAnnotation = new LineAnnotation()
            {
                Type = LineAnnotationType.Vertical,
                Color = OxyColor.FromAColor(100, OxyColors.Black),
                StrokeThickness = resolution,
                YAxisKey = annotationsKey
            };
            model.Annotations.Add(externalSelectAnnotation);

            selectAnnotation = new LineAnnotation()
            {
                Type = LineAnnotationType.Vertical,
                Color = OxyColor.FromAColor(100, OxyColors.Blue),
                StrokeThickness = resolution,
                YAxisKey = annotationsKey,
                X = -1
            };
            model.Annotations.Add(selectAnnotation);

            rangeAnnotation = new RectangleAnnotation()
            {
                StrokeThickness = 0,
                YAxisKey = annotationsKey,
                Fill = OxyColor.FromAColor(50, OxyColors.Gray),
                MinimumX=-2, MaximumX=-1
            };
            model.Annotations.Add(rangeAnnotation);

            var command = new DelegatePlotCommand<OxyMouseEventArgs>(
            (v, c, a) =>
            {
                selectAnnotation.X = Math.Round(dateAxis.InverseTransform(a.Position.X));
                model.InvalidatePlot(false);
                Selected((int)selectAnnotation.X);
            });
            controller.BindMouseDown(OxyMouseButton.Left, command);

            axis.ForEach((a) => model.Axes.Add(a));

            sampleParameters.ForEach((p) => p.AddTo(model));

            valueParameters.ToList().ForEach((pair)=> pair.Value.AddTo(model));
        }

        public void RecalcView(bool invalidate = true)
        {
            if (null != plotModel.PlotView && plotModel.PlotView.ClientArea.Width!=0)
            {
                axisScreenLength = (plotModel.PlotView.ClientArea.Width
                    - dateAxis.ScreenMin.X
                    - (plotModel.IsLegendVisible && plotModel.LegendPlacement == LegendPlacement.Outside ? plotModel.LegendArea.Width : 0)
                    ) / resolution;

                ScrollViewport = axisScreenLength;
                ScrollLarge = ScrollViewport;
                ScrollSmall = ScrollLarge / 10;

                SetPlotViewport();

                if (invalidate) plotModel.InvalidatePlot(false);
            }
        }

        public void PlotterIdx(int idx)
        {
            idxMax = idx;
            AdjustIdxMin();
            if (isLimited) ProcessOnline();
        }

        void AdjustIdxMin()
        {
            if (isLimited && idxMax >= limit - 1) idxMin = idxMax - (limit - 1);
        }

        void ProcessOnline()
        {
            double currPos = scrollPos;

            SetPlotViewport();

            if (isShowNewest)
            {
                if (SamplesNum() > axisScreenLength) RealScrollTo(dateAxis.AbsoluteMaximum, false);
            }
            else
            {
                if (currPos > ScrollMin) RealScrollTo(currPos, false);
                else currPos = ScrollMin;
            }

            plotModel.InvalidatePlot(true);
        }

        void SetPlotViewport()
        {
            double relPos = RelPos(scrollPos);

            if (SamplesNum() > axisScreenLength && idxMin < idxMax)
            {
                dateAxis.AbsoluteMinimum = idxMin;
                dateAxis.AbsoluteMaximum = idxMax;
            }
            else
            {
                dateAxis.AbsoluteMinimum = idxMin;
                dateAxis.AbsoluteMaximum = idxMin + axisScreenLength;
            }

            ScrollMin = dateAxis.AbsoluteMinimum + ScrollViewport / 2;
            ScrollMax = dateAxis.AbsoluteMaximum - ScrollViewport / 2;

            double newScrollPos = AbsPos(relPos);

            InternalScrollPos = SetDataViewport(newScrollPos - ScrollViewport / 2, newScrollPos + ScrollViewport / 2);
        }

        double ScrollTo(double pos, bool invalidate = true)
        {
            if (pos != scrollPos && SamplesNum() > ScrollViewport)
            {
                pos=SetDataViewport(pos - ScrollViewport / 2, pos + ScrollViewport / 2);

                if (invalidate) plotModel.InvalidatePlot(false);
            }

            return pos;
        }

        double SetDataViewport(double min, double max)
        {
            if (min < dateAxis.AbsoluteMinimum)
            {
                min = dateAxis.AbsoluteMinimum;
                max = min + ScrollViewport;
            }
            else if (max > dateAxis.AbsoluteMaximum)
            {
                max = dateAxis.AbsoluteMaximum;
                min = max - ScrollViewport;
            }

            dateAxis.Minimum = min;
            dateAxis.Maximum = max;

            return min + (max - min) / 2;
        }

        void RealScrollTo(double pos, bool invalidate = true)
        {
            InternalScrollPos = ScrollTo(pos, invalidate);
        }

        double RelPos(double abs)
        {
            if (ScrollMax == ScrollMin) return 0;
            else return (abs - ScrollMin) / (ScrollMax - ScrollMin); ;
        }

        double AbsPos(double rel)
        {
            return ScrollMin + rel * (ScrollMax - ScrollMin);
        }

        int SamplesNum()
        {
            return idxMax - idxMin + 1;
        }

        public PlotModel PlotsModel
        {
            get { return plotModel; }
            set { plotModel = value; OnPropertyChanged("PlotsModel"); }
        }

        public IPlotController Controller
        {
            get { return controller; }
        }

        public bool IsShowNewest
        {
            get { return isShowNewest; }
            set { isShowNewest = value; OnPropertyChanged("IsShowNewest"); }
        }

        public bool IsShowLegend
        {
            get { return plotModel.IsLegendVisible; }
            set
            {
                plotModel.IsLegendVisible = value;
                RecalcView();
                OnPropertyChanged("IsShowLegend"); 
            }
        }

        /// <summary>
        /// Ограничено ли количество хранимых данных
        /// </summary>
        public bool IsLimited
        {
            get { return isLimited; }
            set
            {
                if (value != isLimited)
                {
                    if (value) allocator.UseRingBuffer(limit);
                    else allocator.UseList();

                    isLimited = value;
                    OnPropertyChanged("IsLimited");
                }
            }
        }

        public int Limit
        {
            get { return limit; }
            set
            {
                if (value != limit)
                {
                    if (isLimited)
                    {
                        allocator.UseRingBuffer(value); //TODO Expand/Trim current RB
                        idxMin = idxMax;
                        RecalcView();
                    }

                    limit = value;
                    OnPropertyChanged("Limit");
                }
            }
        }

        public double ScrollMin
        {
            get { return scrollMin; }
            protected set { scrollMin = value; OnPropertyChanged("ScrollMin"); }
        }

        public double ScrollMax
        {
            get { return scrollMax; }
            protected set { scrollMax = value; OnPropertyChanged("ScrollMax"); }
        }

        public double ScrollPos
        {
            get { return scrollPos; }
            set { scrollPos = ScrollTo(value); }
        }

        double InternalScrollPos
        {
            get { return scrollPos; }
            set { scrollPos = value; OnPropertyChanged("ScrollPos"); }
        }

        public double ScrollViewport
        {
            get { return scrollViewport; }
            protected set { scrollViewport = value; OnPropertyChanged("ScrollViewport"); }
        }

        public double ScrollLarge
        {
            get { return scrollLarge; }
            protected set { scrollLarge = value; OnPropertyChanged("ScrollLarge"); }
        }

        public double ScrollSmall
        {
            get { return scrollSmall; }
            protected set { scrollSmall = value; OnPropertyChanged("ScrollSmall"); }
        }

        public void Select(int idx0, int idx1)
        {
          
            if (idx1 < dateAxis.Minimum || idx0 > dateAxis.Maximum) RealScrollTo(idx0+(idx1 - idx0) / 2, false);

            rangeAnnotation.MinimumX = idx0;
            rangeAnnotation.MaximumX = idx1;

            plotModel.InvalidatePlot(false);
        }

        #region Features.ISelectable

        void Features.ISingleSelectable.Select(int idx)
        {
            if (externalSelectAnnotation.X == idx) return;

            if(idx<dateAxis.Minimum || idx> dateAxis.Maximum) RealScrollTo( idx, false);

            externalSelectAnnotation.X = idx;
            plotModel.InvalidatePlot(false);
        }

        void Features.ISelectable.Unselect()
        {
            UnselectExternal();
            plotModel.InvalidatePlot(false);
        }

        void UnselectExternal()
        {
            externalSelectAnnotation.X = -1;
        }

        void UnselectInternal()
        {
            selectAnnotation.X = -1;
        }

        void Unselect()
        {
            UnselectExternal();
            UnselectInternal();

            rangeAnnotation.MinimumX = -2;
            rangeAnnotation.MaximumX = -1;
        }

        public event Action<int> Selected = (i) => { };
        #endregion

        public Plotter PlotterFor(string valueName)
        {
            if (valueParameters.ContainsKey(valueName)) return valueParameters[valueName].Add;
            else return null;
        }

        #region PVAllocator

        protected class PVAllocator : OP.ReAllocator
        {
            Func<IList<DataPoint>> allocator;
            int rbCapacity;

            public PVAllocator(int defaultRBCapacity)
            {
                UseList();
                rbCapacity = defaultRBCapacity;
            }

            IList<DataPoint> ListAllocator()
            {
                return new List<DataPoint>();
            }

            IList<DataPoint> RingAllocator()
            {
                return new Utils.RingBuffer<DataPoint>(rbCapacity);
            }

            public void UseRingBuffer(int capacity)
            {
                rbCapacity = capacity;
                allocator = RingAllocator;     //TODO Изменение размеров RingBuffer

                ReallocateAll();
            }

            public void UseList()
            {
                allocator = ListAllocator;

                ReallocateAll();
            }

            #region ReAllocator child iface

            protected override IList<DataPoint> Allocation()
            {
                return allocator();
            }
            #endregion
        }
        #endregion
    }
}
