﻿
using System;
using System.Collections.Generic;
using Meta.Numerics.Statistics;
using System.Collections.ObjectModel;
using Ula.Utils;

namespace Ula.Features.LogPlots
{
    public class IPStatsCollector
    {
        public event Action<int, double> windowAvg = (i, v) => { };
        public event Action<int, double> windowSigma = (i, v) => { };
        public event Action<int, double> windowGCSigma = (i, v) => { };
        public event Action<int, double> windowLCorr = (i, v) => { };
        public event Action<int, double> windowRCorr = (i, v) => { };
        public event Action<int, double> windowLCorr2 = (i, v) => { };
        public event Action<int, double> windowRCorr2 = (i, v) => { };
        public event Action<int> windowEnd = (i) => { };

        const int windowW = 60;
        const int windowS = 10;

        Sample fpsSample = new Sample();
        Sample fpsWindowSample = new Sample();

        BivariateSample frameTgcSample = new BivariateSample();
        BivariateSample frameTgcWindowSample = new BivariateSample();

        BivariateSample frameTgcOnlySample = new BivariateSample();
        BivariateSample frameTgcOnlyWindowSample = new BivariateSample();

        Histogram fpsHist = new Histogram(new List<double>() { 0, 35, 45, 55, 65, 100 });
        
        int gcCount = 0;
        double gcTime = 0;
        double maxMem = 0;

        int windowIdx = 0;

        Utils.RingBuffer<Unity.IPSample> windowRB = new Utils.RingBuffer<Unity.IPSample>(windowW);

        public void Add(Unity.IPSample sample)
        {
            frameTgcSample.Add(sample.frametime.avg, sample.mono_memory.collection_total_duration);
            if (sample.mono_memory.collection_total_duration > 0)
                frameTgcOnlySample.Add(sample.frametime.avg, sample.mono_memory.collection_total_duration);

            double fps = Utils.Utils.ToFPS(sample.frametime.avg);
            fpsSample.Add(fps);
            fpsHist.Add(fps);

            gcCount += sample.mono_memory.max_number_of_collections;
            gcTime += sample.mono_memory.collection_total_duration;
            maxMem = Math.Max(sample.mono_memory.allocated_heap, maxMem);

            windowRB.Add(sample);
            if (windowRB.Count == windowW)
            {
                if (0 == fpsSample.Count % windowS)
                {
                    fpsWindowSample.Clear();
                    frameTgcWindowSample.Clear();
                    frameTgcOnlyWindowSample.Clear();

                    windowRB.ForEach((s) =>
                        {
                            fpsWindowSample.Add(Utils.Utils.ToFPS(s.frametime.avg));
                            frameTgcWindowSample.Add(s.frametime.avg, s.mono_memory.collection_total_duration);
                            if (s.mono_memory.collection_total_duration > 0)
                                frameTgcOnlyWindowSample.Add(s.frametime.avg, s.mono_memory.collection_total_duration);
                        });
                    
                    windowAvg(windowIdx,   fpsWindowSample.Mean);
                    windowSigma(windowIdx, fpsWindowSample.StandardDeviation);

                    windowGCSigma(windowIdx, frameTgcWindowSample.Y.StandardDeviation);

                    windowLCorr(windowIdx, frameTgcWindowSample.PearsonRTest().Statistic);
                    windowRCorr(windowIdx, frameTgcWindowSample.SpearmanRhoTest().Statistic);

                    if (frameTgcOnlyWindowSample.Count > 5)
                    { 
                        windowLCorr2(windowIdx, frameTgcOnlyWindowSample.PearsonRTest().Statistic);
                        windowRCorr2(windowIdx, frameTgcOnlyWindowSample.SpearmanRhoTest().Statistic);
                    }

                    windowEnd(windowIdx);

                    ++windowIdx;
                }
            }
        }

        public void Clear()
        {
            fpsSample.Clear();
            frameTgcSample.Clear();
            frameTgcOnlySample.Clear();
            fpsHist.Clear();
            gcCount = 0;
            gcTime = 0;
            maxMem = 0;
            windowIdx = 0;
        }


        public ObservableCollection<Tuple<string, double>> FPSStats()
        {
            return FPSStats(fpsSample);
        }

        public HistogramBinsCollection FPSHistogramBins
        {
            get { return fpsHist.Bins; }
        }

        public int SrcIdx(int sidx)
        {
            return windowW - 1 + sidx * windowS;
        }

        ObservableCollection<Tuple<string, double>> FPSStats(Meta.Numerics.Statistics.Sample sample)
        {
            ObservableCollection<Tuple<string, double>> stats = new ObservableCollection<Tuple<string, double>>();
            try
            {
                stats.Add(new Tuple<string, double>("Mean (x\u0305)", sample.Mean));
                stats.Add(new Tuple<string, double>("Std. Deviation (\u03C3)", sample.StandardDeviation));
                stats.Add(new Tuple<string, double>("Min", sample.Minimum));
                stats.Add(new Tuple<string, double>("Max", sample.Maximum));
                var i = sample.InterquartileRange;
                stats.Add(new Tuple<string, double>(".25->", i.LeftEndpoint));
                stats.Add(new Tuple<string, double>("<-.75", i.RightEndpoint));
                stats.Add(new Tuple<string, double>("Samples #", sample.Count));
            }
            catch { }

            return stats;
        }

        public ObservableCollection<Tuple<string, double>> GCStats()
        {
            ObservableCollection<Tuple<string, double>> stats = new ObservableCollection<Tuple<string, double>>();
            try
            {
                stats.Add(new Tuple<string, double>("Garbage collections #", gcCount));
                stats.Add(new Tuple<string, double>("GC total time, sec", gcTime / 1000));
                stats.Add(new Tuple<string, double>("Max allocated heap", maxMem));

                var r = frameTgcSample.PearsonRTest();
                stats.Add(new Tuple<string, double>("Frametime&GC Pearson correlation", r.Statistic));

                var s = frameTgcSample.SpearmanRhoTest();
                stats.Add(new Tuple<string, double>("Frametime&GC Spearman correlation", s.Statistic));

                var ro = frameTgcOnlySample.PearsonRTest();
                stats.Add(new Tuple<string, double>("Frametime@GC Pearson correlation", ro.Statistic));

                var so = frameTgcOnlySample.SpearmanRhoTest();
                stats.Add(new Tuple<string, double>("Frametime@GC Spearman correlation", so.Statistic));
#if DEBUG
                stats.Add(new Tuple<string, double>("Frametime&GC Correlation", frameTgcSample.CorrelationCoefficient));
                stats.Add(new Tuple<string, double>("Frametime@GC Correlation", frameTgcOnlySample.CorrelationCoefficient));

                stats.Add(new Tuple<string, double>("r Left Probability", r.LeftProbability));
                stats.Add(new Tuple<string, double>("r Right Probability", r.RightProbability));
                stats.Add(new Tuple<string, double>("r @ Left Probability", ro.LeftProbability));
                stats.Add(new Tuple<string, double>("r @ Right Probability", ro.RightProbability));

                stats.Add(new Tuple<string, double>("\u03C1 Left Probability", s.LeftProbability));
                stats.Add(new Tuple<string, double>("\u03C1 Right Probability", s.RightProbability));
                stats.Add(new Tuple<string, double>("\u03C1 @ Left Probability", so.LeftProbability));
                stats.Add(new Tuple<string, double>("\u03C1 @ Right Probability", so.RightProbability));
#endif
            }
            catch { }

            return stats;
        }

        public int WindowW
        {
            get { return windowW; }
        }
    }
}
