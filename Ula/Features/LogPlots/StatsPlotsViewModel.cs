﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using OxyPlot;
using OxyPlot.Axes;
using Ula.Plots;
using OxyPlot.Annotations;
using System.Linq;

namespace Ula.Features.LogPlots
{
    class StatsPlotsViewModel : PlotsViewModel
    {
        readonly static string avgKey =   "avg";
        readonly static string sigmaKey = "sigma";
        readonly static string corrKey =  "corr";

        public StatsPlotsViewModel()
            :base(new List<LinearAxis>()
            {
            new LinearAxis() { Key = avgKey,
                EndPosition =   1,
                StartPosition = 0, Position = AxisPosition.Left, Title = "FPS x\u0305", TitleColor = OxyColors.Red,
                AbsoluteMinimum = 30, Minimum = 30, IsAxisVisible = true, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid, PositionTier=0, FilterMaxValue = 100, Maximum=65
            },
            new LinearAxis() { Key = sigmaKey,
                EndPosition =   1,
                StartPosition = 0, Position = AxisPosition.Left, Title = "\u03C3", 
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = true, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid, PositionTier=1,
            },
            new LinearAxis() { Key = corrKey,
                EndPosition =   1,
                StartPosition = 0, Position = AxisPosition.Left, Title = "r, \u03C1",
                AbsoluteMinimum = -0.5, Minimum = -0.5, AbsoluteMaximum = 1, Maximum = 1,
                IsAxisVisible = true, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid, PositionTier=2,
            },
            })
        {
        }

        override protected void InitChild()
        {
            Parameters(new List<IPlotIPSampleSeries>()
            {
            },
            new Dictionary<string, IPlotValueSeries>()
            {
                {"FPSavg", new ValueSeries(avgKey, "FPS x\u0305", OxyColors.Red, allocator, LineStyle.Solid, "{0}\n{4:N1}")},
                {"FPSsigma", new ValueSeries(sigmaKey, "FPS \u03C3", OxyColors.Magenta, allocator, LineStyle.Solid, "{0}\n{4:N}")},
                {"GCsigma",  new ValueSeries(sigmaKey, "GC \u03C3", OxyColors.Brown,   allocator, LineStyle.Solid, "{0}\n{4:N}")},
                {"FT&GCLinear", new ValueSeries(corrKey, "Frametime&GC r",      OxyColors.Blue,   allocator, LineStyle.Solid, "{0}\n{4:N2}")},
                {"FT&GCRank",   new ValueSeries(corrKey, "Frametime&GC \u03C1", OxyColors.Green,  allocator, LineStyle.Solid, "{0}\n{4:N2}")},
                {"FT@GCLinear", new ValueSeries(corrKey, "Frametime@GC r",      OxyColors.Yellow, allocator, LineStyle.Dash,  "{0}\n{4:N2}")},
                {"FT@GCRank",   new ValueSeries(corrKey, "Frametime@GC \u03C1", OxyColors.Orange, allocator, LineStyle.Dash,  "{0}\n{4:N2}")}
            });
        }
    }
}
