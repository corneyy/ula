﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using OxyPlot;
using OxyPlot.Axes;
using Ula.Plots;
using OxyPlot.Annotations;
using System.Linq;

namespace Ula.Features.LogPlots
{
    class OverviewPlotsViewModel : PlotsViewModel
    {
        readonly static string cpuKey =       "cpu";
        readonly static string scriptsKey =   "scripts";
        readonly static string memoryKey =    "memory";
        readonly static string logsPerSamKey = "logsPSam";

        public OverviewPlotsViewModel()
            :base(new List<LinearAxis>()
            {
            new LinearAxis() { Key = cpuKey,
                EndPosition =   1,
                StartPosition = 0, Position = AxisPosition.Left, Title = "FPS", TitleColor = OxyColors.Red,
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = true, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid, PositionTier=0, FilterMaxValue = 100,
            },
            new LinearAxis() { Key = scriptsKey,
                EndPosition =   1,
                StartPosition = 0, Position = AxisPosition.Left, Title = "GC Total", TitleColor = OxyColors.Blue,
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = true, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid, PositionTier=1,
            },
            new LinearAxis() { Key = logsPerSamKey,
                EndPosition =   1,
                StartPosition = 0, Position = AxisPosition.Right,
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = false, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid,
            },
            new LinearAxis() { Key = memoryKey,
                EndPosition =   1,
                StartPosition = 0, Position = AxisPosition.Left, Title = "Memory",
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = true, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid,  PositionTier=2,
            },
            })
        {
        }

        override protected void InitChild()
        {
            Parameters(new List<IPlotIPSampleSeries>()
            {
                new StatSeries(cpuKey, "FPS",     (s) => ToFPS(s.frametime) ,     OxyColors.Red,     allocator, "{0}\n{4}"),

                new ParameterSeries(scriptsKey, "GC total", (s) => s.mono_memory.collection_total_duration, OxyColors.Blue, allocator),

                new ParameterSeries(memoryKey, "allocated heap", (s) => Math.Round((double) s.mono_memory.allocated_heap/1024/1024, 1), OxyColors.Orange,   allocator),
                new ParameterSeries(memoryKey, "used heap",      (s) => Math.Round((double) s.mono_memory.used_heap/1024/1024, 1),      OxyColors.Green, allocator),

            },
            new Dictionary<string, IPlotValueSeries>()
            {
                {"LogsPerSample",new ValueSeries(logsPerSamKey, "logs #", OxyColors.Magenta, allocator, LineStyle.Dot)}
            });
        }

        Unity.IPSample.Stat ToFPS(Unity.IPSample.Stat stat)
        {
            return new Unity.IPSample.Stat()
            {
                min = Utils.Utils.ToFPS(stat.min),
                max = Utils.Utils.ToFPS(stat.max),
                avg = Utils.Utils.ToFPS(stat.avg)
            };
        }
    }
}
