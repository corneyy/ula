﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using OxyPlot;
using OxyPlot.Axes;
using Ula.Plots;
using OxyPlot.Annotations;
using System.Linq;

namespace Ula.Features.LogPlots
{
    public class DetailPlotsViewModel : PlotsViewModel
    {
        readonly static string cpuKey =       "cpu";
        readonly static string renderKey =    "render";
        readonly static string playerKey =    "player";
        readonly static string playerFUCKey = "playerFUC";
        readonly static string scriptsKey =   "scripts";
        readonly static string memoryKey =    "memory";
        readonly static string memoryNoCKey = "memoryNoC";
        readonly static string logsPerSamKey = "logsPSam";
        
        public DetailPlotsViewModel():
            base(new List<LinearAxis>()
            {
            new LinearAxis() { Key = cpuKey,
                EndPosition =   1,
                StartPosition = 0.68, Position = AxisPosition.Left, Title = "General CPU Activity",
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = true, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid, FilterMaxValue = 40,
            },
            new LinearAxis() { Key = renderKey, 
                EndPosition =   0.67, 
                StartPosition = 0.48, Position = AxisPosition.Left, Title = "Rendering",
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = true, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid, FilterMaxValue = 10000,
            },
            new LinearAxis() { Key = playerFUCKey,
                EndPosition =   0.47,
                StartPosition = 0.28, Position = AxisPosition.Right,
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = false, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid,
            },
            new LinearAxis() { Key = playerKey,
                EndPosition =   0.47,
                StartPosition = 0.28, Position = AxisPosition.Left, Title = "Unity Player",
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = true, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid,
            },
            new LinearAxis() { Key = scriptsKey,
                EndPosition =   0.27,
                StartPosition = 0.11, Position = AxisPosition.Left, Title = "Scripts+GC",
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = true, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid,
            },
            new LinearAxis() { Key = logsPerSamKey,
                EndPosition =   0.10,
                StartPosition = 0, Position = AxisPosition.Right,
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = false, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid,
            },
            new LinearAxis() { Key = memoryNoCKey,
                EndPosition =   0.10,
                StartPosition = 0, Position = AxisPosition.Right,
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = false, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid,
            },
            new LinearAxis() { Key = memoryKey,
                EndPosition =   0.10,
                StartPosition = 0, Position = AxisPosition.Left, Title = "Mem+GC",
                AbsoluteMinimum = 0, Minimum = 0, IsAxisVisible = true, IsZoomEnabled = false, IsPanEnabled = false,
                AxislineStyle = LineStyle.Solid,
            },
            })
        {
        }

        protected override void InitChild()
        {
            Parameters(new List<IPlotIPSampleSeries>()
            {
                new StatSeries(cpuKey, "cpu-player",    (s) => s.cpu_player,    OxyColors.Blue,    allocator),
                new StatSeries(cpuKey, "cpu-ogles-drv", (s) => s.cpu_ogles_drv, OxyColors.Green,   allocator),
                new StatSeries(cpuKey, "frametime",     (s) => s.frametime,     OxyColors.Red,     allocator),
                new StatSeries(cpuKey, "cpu-waits-gpu", (s) => s.cpu_waits_gpu, OxyColors.Orange,  allocator),
                new StatSeries(cpuKey, "msaa-resolve",  (s) => s.msaa_resolve,  OxyColors.Cyan,    allocator),
                new StatSeries(cpuKey, "cpu-present",   (s) => s.cpu_present,   OxyColors.Magenta, allocator),
                
                new StatBatchedSeries(renderKey, "draw-call #", (s) => s.draw_call, OxyColors.Red,   allocator),
                new StatBatchedSeries(renderKey, "tris #",      (s) => s.tris,      OxyColors.Green, allocator),
                new StatBatchedSeries(renderKey, "verts #",     (s) => s.verts,     OxyColors.Blue,  allocator),

                new RangeSeries(playerFUCKey, "Fix Upd count", (s) => 
                    new RangeSeries.Range(s.player_detail.fixed_update_count_min, s.player_detail.fixed_update_count_max) , OxyColors.Brown, allocator),

                new ParameterSeries(playerKey, "physx",     (s) => s.player_detail.physx,     OxyColors.Red,     allocator),
                new ParameterSeries(playerKey, "animation", (s) => s.player_detail.animation, OxyColors.Green,   allocator),
                new ParameterSeries(playerKey, "culling",   (s) => s.player_detail.culling,   OxyColors.Orange,  allocator),
                new ParameterSeries(playerKey, "skinning",  (s) => s.player_detail.skinning,  OxyColors.Cyan,    allocator),
                new ParameterSeries(playerKey, "batching",  (s) => s.player_detail.batching,  OxyColors.Magenta, allocator),
                new ParameterSeries(playerKey, "render",    (s) => s.player_detail.render,    OxyColors.Blue,    allocator),

                new ParameterSeries(scriptsKey, "update",      (s) => s.mono_scripts.update,      OxyColors.Red,   allocator),
                new ParameterSeries(scriptsKey, "fixedUpdate", (s) => s.mono_scripts.fixedUpdate, OxyColors.Green, allocator),
                new ParameterSeries(scriptsKey, "coroutines",  (s) => s.mono_scripts.coroutines,  OxyColors.Blue,  allocator),

                new ParameterSeries(scriptsKey, "GC total", (s) => s.mono_memory.collection_total_duration, OxyColors.Orange, allocator),

                new ParameterSeries(memoryKey, "allocated heap", (s) => Math.Round((double) s.mono_memory.allocated_heap/1024/1024, 1), OxyColors.Red,   allocator),
                new ParameterSeries(memoryKey, "used heap",      (s) => Math.Round((double) s.mono_memory.used_heap/1024/1024, 1),      OxyColors.Green, allocator),

                new ParameterSeries(memoryNoCKey, "max # GC", (s) => s.mono_memory.max_number_of_collections, OxyColors.Blue, allocator)
            },
            new Dictionary<string, IPlotValueSeries>()
            {
                {"LogsPerSample",new ValueSeries(logsPerSamKey, "logs #", OxyColors.Magenta, allocator, LineStyle.Dot)}
            });
        }
    }
}
