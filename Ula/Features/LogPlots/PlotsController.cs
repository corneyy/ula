﻿using System;
using System.Collections.ObjectModel;
using Ula.Utils;

namespace Ula.Features.LogPlots
{
    public delegate void Plotter(int idx, double value);

    class PlotsController : Utils.NotifyPropertyChanged, Features.ISingleSelectable
    {
        const int updateInterval = 5;

        bool isShowNewest = true;
        int limit = 1000;
        bool isLimited = false;
        bool isShowLegend = false;

        OverviewPlotsViewModel overviewVM;
        DetailPlotsViewModel detailVM;
        StatsPlotsViewModel statsVM;
        HistogramViewModel fpsHistVM;

        Ula.UI.LogPlots logPlots;

        IPStatsCollector stats = new IPStatsCollector();

        ObservableCollection<Tuple<string, double>> fpsStats;
        ObservableCollection<Tuple<string, double>> gcStats;

        public PlotsController(Ula.UI.LogPlots logPlots)
        {
            overviewVM = new OverviewPlotsViewModel()
            {
                IsShowNewest = isShowNewest,
                Limit = limit,
                IsLimited = isLimited,
                IsShowLegend = isShowLegend
            };

            detailVM = new DetailPlotsViewModel()
            {
                IsShowNewest = isShowNewest,
                Limit = limit,
                IsLimited = isLimited,
                IsShowLegend = isShowLegend
            };

            statsVM = new StatsPlotsViewModel()
            {
                IsShowNewest = isShowNewest,
                Limit = limit,
                IsLimited = isLimited,
                IsShowLegend = isShowLegend
            };

            statsVM.Selected += (idx) =>
                {
                    int idx0 = stats.SrcIdx(idx);
                    int idx1 = idx0 - stats.WindowW + 1;
                    overviewVM.Select(idx0, idx1);
                    detailVM.Select(idx0, idx1);
                };
 
            stats.windowAvg += new Action<int, double>(statsVM.PlotterFor("FPSavg"));
            stats.windowSigma += new Action<int, double>(statsVM.PlotterFor("FPSsigma"));
            stats.windowGCSigma += new Action<int, double>(statsVM.PlotterFor("GCsigma"));
            stats.windowLCorr += new Action<int, double>(statsVM.PlotterFor("FT&GCLinear"));
            stats.windowRCorr += new Action<int, double>(statsVM.PlotterFor("FT&GCRank"));
            stats.windowLCorr2 += new Action<int, double>(statsVM.PlotterFor("FT@GCLinear"));
            stats.windowRCorr2 += new Action<int, double>(statsVM.PlotterFor("FT@GCRank"));
            stats.windowEnd += (idx) => statsVM.PlotterIdx(idx);

            fpsHistVM = new HistogramViewModel("FPS histogram");

            this.logPlots = logPlots;

            logPlots.SizeChanged += (a, b) => RecalcView();
            logPlots.DStatistics.DataContext = this;

            logPlots.FPSHistogram.DataContext = fpsHistVM;
            logPlots.OverviewPlots.DataContext = overviewVM;
            logPlots.Details.DataContext = detailVM;
            logPlots.StatsPlots.DataContext = statsVM;
        }

        public void NewSampleOnline(int idx, Unity.IPSample sample)
        {
            overviewVM.NewSampleOnline(idx, sample);
            detailVM.NewSampleOnline(idx, sample);

            stats.Add(sample);
            
            if (0 == idx % updateInterval) UpdateHist();
        }

        public void NewSampleOffline(int idx, Unity.IPSample sample)
        {
            overviewVM.NewSampleOffline(idx, sample);
            detailVM.NewSampleOffline(idx, sample);

            stats.Add(sample);
        }

        public void Offlined()
        {
            overviewVM.Offlined();
            detailVM.Offlined();
            statsVM.Offlined();

            UpdateHist();
        }

        public void Clear()
        {
            overviewVM.Clear();
            detailVM.Clear();
            statsVM.Clear();

            stats.Clear();
            fpsHistVM.Clear();
        }

        public void RecalcView(bool invalidate = true)
        {
            overviewVM.RecalcView(invalidate);
            detailVM.RecalcView(invalidate);
            statsVM.RecalcView(invalidate);
        }

        void UpdateHist()
        {
            FPSStatistics = stats.FPSStats();
            GCStatistics = stats.GCStats();

            fpsHistVM.New((h) =>
            {
                stats.FPSHistogramBins.ForEach((b) =>
                {
                    h.Add(b.Range.LeftEndpoint + "<->" + b.Range.RightEndpoint, b.Counts);
                });

            });
        }

        public bool IsShowNewest
        {
            get { return isShowNewest; }
            set 
            {
                isShowNewest = value;
                overviewVM.IsShowNewest = value;
                detailVM.IsShowNewest = value;
                statsVM.IsShowNewest = value; 
                OnPropertyChanged("IsShowNewest"); 
            }
        }

        public bool IsShowLegend
        {
            get { return isShowLegend; }
            set
            {
                isShowLegend = value;
                overviewVM.IsShowLegend = value;
                detailVM.IsShowLegend = value;
                statsVM.IsShowLegend = value;
                OnPropertyChanged("IsShowLegend"); 
            }
        }

        /// <summary>
        /// Ограничено ли количество хранимых данных
        /// </summary>
        public bool IsLimited
        {
            get { return isLimited; }
            set
            {
                if (value != isLimited)
                {
                    isLimited = value;
                    overviewVM.IsLimited = value;
                    detailVM.IsLimited = value;
                    statsVM.IsLimited = value;
                    OnPropertyChanged("IsLimited");
                }
            }
        }

        public int Limit
        {
            get { return limit; }
            set
            {
                if (value != limit)
                {
                    limit = value;
                    overviewVM.Limit = value;
                    detailVM.Limit = value;
                    statsVM.Limit = value;
                    OnPropertyChanged("Limit");
                }
            }
        }

        public bool IsDetailed
        {
            get { return logPlots.SelectedIndex == 1; }
            set { logPlots.SelectedIndex = value ? 1 : 0; OnPropertyChanged("IsDetailed"); }
        }

        public ObservableCollection<Tuple<string, double>> FPSStatistics
        {
            get { return fpsStats; }
            set { fpsStats = value; OnPropertyChanged("FPSStatistics"); }
        }

        public ObservableCollection<Tuple<string, double>> GCStatistics
        {
            get { return gcStats; }
            set { gcStats = value; OnPropertyChanged("GCStatistics"); }
        }

        #region Features.ISelectable

        void Features.ISingleSelectable.Select(int idx)
        {
            (overviewVM as Features.ISingleSelectable).Select(idx);
            (detailVM as Features.ISingleSelectable).Select(idx);
        }

        void Features.ISelectable.Unselect()
        {
            (overviewVM as Features.ISelectable).Unselect();
            (detailVM as Features.ISelectable).Unselect();
        }

        public event Action<int> Selected
        {
            add { detailVM.Selected += value; overviewVM.Selected += value; }
            remove { detailVM.Selected -= value; overviewVM.Selected -= value; }
        }
        #endregion

        public Plotter PlotterFor(string valueName)
        {
            Plotter o = overviewVM.PlotterFor(valueName); 
            Plotter d = detailVM.PlotterFor(valueName);
            if (null == o && null == d) return null;
            else if (null == o) return d;
            else if (null == d) return o;
            else return (idx, value) => { o(idx, value); d(idx, value); };
        }
    }
}
