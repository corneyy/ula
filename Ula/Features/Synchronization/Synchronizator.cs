﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ula.Features.Synchronization
{
    using Pair = Tuple<int, DateTime>;

    public class Synchronizator
    {
        public const int invalidIdx = -1;
        public static readonly IList<int> invalidList = new List<int>();
    }

    public class UnlimitedSynchronizator : Synchronizator, SamplesLinesSync.ISynchronizator
    {
        int sampleIdx=0;
        int lineIdx=0;
        const int defaultMaxAge = 10; //seconds
        const int initMapCapacity = 1000;

        Queue<Pair> lines = new Queue<Pair>();

        Dictionary<int, List<int>> sample2line = new Dictionary<int, List<int>>(initMapCapacity);
        Dictionary<int, int> line2sample = new Dictionary<int, int>(initMapCapacity);
        
        TimeSpan maxAge; 
        TimeSpan sampleSpan = new TimeSpan(0, 0, 1); //seconds

        public UnlimitedSynchronizator() : this(defaultMaxAge) { }

        public UnlimitedSynchronizator(int maxAgeInSeconds)
        {
            this.maxAge = new TimeSpan(0, 0, maxAgeInSeconds);
        }

        #region ISynchronizator

        public int NextSampleIdx(DateTime dt)
        {
            if (IsValid(dt)) Sync(sampleIdx, dt);
            return sampleIdx++;
        }

        public int NextLineIdx(DateTime dt)
        {
            if (IsValid(dt)) AddLine(lineIdx, dt);
            return lineIdx++;
        }

        public IList<int> LineIdx(int sampleIdx)
        {
            if (sample2line.ContainsKey(sampleIdx)) return sample2line[sampleIdx];
            return invalidList;
        }

        public int SampleIdx(int lineIdx)
        {
            if (line2sample.ContainsKey(lineIdx)) return line2sample[lineIdx];
            return invalidIdx;
        }
        #endregion

        static bool IsValid(DateTime dt)
        {
            return null != dt && dt != DateTime.MinValue;
        }

        void AddLine(int idx, DateTime dt)
        {
            lines.Enqueue(new Pair(idx, dt));
            ClearAgedLines(dt);
        }

        void ClearAgedLines(DateTime newest)
        {
            while (lines.Count > 0 && lines.Peek().Item2 < newest - maxAge) lines.Dequeue();
        }

        void Sync(int sampleIdx, DateTime sampleDT)
        {
            while (lines.Count > 0 && lines.Peek().Item2 <= sampleDT)
            {
                Pair p=lines.Dequeue();
                if (p.Item2 >= sampleDT - sampleSpan) Sync(sampleIdx, p.Item1);
            }
        }

        void Sync(int sampleIdx, int lineIdx)
        {
            if (!sample2line.ContainsKey(sampleIdx)) sample2line[sampleIdx] = new List<int>();
            sample2line[sampleIdx].Add(lineIdx);

            line2sample[lineIdx] = sampleIdx;
        }

        protected void RemoveSample(int idx, int lineIdx = Synchronizator.invalidIdx)
        {
            if (Synchronizator.invalidIdx != lineIdx 
                && sample2line.ContainsKey(idx) 
                && sample2line[idx].Count>1)
            {
                sample2line[idx].Remove(lineIdx);
            }
            else sample2line.Remove(idx);
        }

        protected void RemoveLine(int idx)
        {
            line2sample.Remove(idx);
        }
    }

    public class LimitedSynchronizator : UnlimitedSynchronizator, SamplesLinesSync.ISynchronizator
    {
        int samplesLimit;
        int linesLimit;
        Queue<int> sampleIdxs;
        Queue<int> lineIdxs;

        public LimitedSynchronizator(int samplesLimit, int linesLimit)
        {
            this.samplesLimit = samplesLimit;
            this.linesLimit = linesLimit;

            sampleIdxs = new Queue<int>(samplesLimit);
            lineIdxs = new Queue<int>(linesLimit);
        }

        int SamplesLinesSync.ISynchronizator.NextSampleIdx(DateTime dt)
        {
            return NextSample(dt, base.NextSampleIdx, sampleIdxs, samplesLimit, 
                (sampleIdx) =>
                {
                    foreach (int lineIdx in LineIdx(sampleIdx))
                        RemoveLine(lineIdx);
                    RemoveSample(sampleIdx);
                });
        }

        int SamplesLinesSync.ISynchronizator.NextLineIdx(DateTime dt)
        {
            return NextSample(dt, base.NextLineIdx, lineIdxs, linesLimit,
                (lineIdx) =>
                {
                    RemoveSample(SampleIdx(lineIdx), lineIdx);                
                    RemoveLine(lineIdx);
                });
        }

        int NextSample(DateTime dt, Func<DateTime, int> next, Queue<int> idxs, int limit, Action<int> remove)
        {
            int idx = next(dt);

            idxs.Enqueue(idx);
            if (idx > limit) remove(idxs.Dequeue());

            return idx;
        }
    }
}
