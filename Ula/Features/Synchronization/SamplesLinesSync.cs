﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ula.Features.Synchronization
{
    public class SamplesLinesSync : Utils.NotifyPropertyChanged
    {
        public interface ISynchronizator
        {
            int NextSampleIdx(DateTime dt);
            int NextLineIdx(DateTime dt);
            IList<int> LineIdx(int sampleIdx);
            int SampleIdx(int lineIdx);
        }

#if DEBUG
        ISynchronizator syncer;
#else
        ISynchronizator syncer=new UnlimitedSynchronizator();
#endif
        ISingleSelectable samples;
        IMultiSelectable lines;
        bool synced;

        public SamplesLinesSync(ISingleSelectable samples, IMultiSelectable lines)
        {
            this.samples = samples;
            this.lines = lines;
            IsSynced = true;
        }

        public ISynchronizator NewUnlimitedSync()
        {
            return syncer = new UnlimitedSynchronizator();
        }

        public ISynchronizator NewLimitedSync(int samplesLimit, int linesLimit)
        {
            return syncer = new LimitedSynchronizator(samplesLimit, linesLimit);
        }

        public bool IsSynced
        {
            get {return synced;}
            set
            {
                if(synced!=value)
                {
                    if(value)
                    {
                        samples.Selected += samples_Selected;
                        lines.Selected += lines_Selected;
                    }
                    else
                    {
                        samples.Selected -= samples_Selected;
                        lines.Selected -= lines_Selected;
                    }

                    synced = value;

                    OnPropertyChanged("IsSynced");
                }
            }
        }

        void lines_Selected(int lineIdx)
        {
            int sampleIdx=syncer.SampleIdx(lineIdx);
            if (Synchronizator.invalidIdx != sampleIdx) samples.Select(sampleIdx);
            else samples.Unselect();
        }

        void samples_Selected(int sampleIdx)
        {
            IList<int> list = syncer.LineIdx(sampleIdx);
            if (Synchronizator.invalidList != list) lines.Select(list);
            else lines.Unselect();
        }
    }
}
