﻿using System;
using System.Collections.Generic;

namespace Ula.Features
{
    public interface ISelectable
    {
        event Action<int> Selected;
        void Unselect();
    }

    public interface ISingleSelectable : ISelectable
    {
        void Select(int idx);
    }

    public interface IMultiSelectable : ISelectable
    {
        void Select(IList<int> list);
    }
}
