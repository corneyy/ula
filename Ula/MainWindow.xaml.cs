﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Threading;

namespace Ula
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        const int maxLimit = 3000;

        string titleAddition;
        bool isSliderVisible;
        DispatcherTimer sliderVisibilityTimer;
        Sources.AdbLogcatOnlineSourceProvider onlineSources;
        Sources.ISource currentSource;
        ListBoxAutomationPeer svAutomation;
        bool cleaning;

        Features.LogPlots.PlotsController plotsVM;
        LogViewModel logVM;

        Features.Synchronization.SamplesLinesSync sync;

        Features.LogPlots.Plotter logsPlotter;

        public MainWindow()
        {
            InitializeComponent();

            sliderVisibilityTimer = new DispatcherTimer();
            sliderVisibilityTimer.Interval = TimeSpan.FromSeconds(1);
            sliderVisibilityTimer.Tick += delegate
            {
                IsSliderVisible = false;
                sliderVisibilityTimer.Stop();
            };

            logVM = new LogViewModel(ScrollTo);

            plotsVM = new Features.LogPlots.PlotsController(this.LogPlots);

            this.DataContext = this;

            AutoPlot.DataContext = plotsVM;
            AutoPlotSlider.DataContext = plotsVM;
            PlotLimit.DataContext = plotsVM;
            ShowLegend.DataContext = plotsVM;
            ShowLegendSlider.DataContext = plotsVM;
            Details.DataContext = plotsVM;
            DetailsSlider.DataContext = plotsVM;
            
            LogView.DataContext = logVM;
            AutoLog.DataContext = logVM;
            AutoLogSlider.DataContext = logVM;
            LogLimit.DataContext = logVM;

            logsPlotter = plotsVM.PlotterFor("LogsPerSample");

            sync = new Features.Synchronization.SamplesLinesSync(plotsVM, logVM);

            svAutomation = (ListBoxAutomationPeer)ScrollViewerAutomationPeer.CreatePeerForElement(LogView);

            onlineSources = new Sources.AdbLogcatOnlineSourceProvider(Dispatcher);

            if (Properties.Settings.Default.AdbPath == "setup") this.Loaded += (s, e) => InitAdbPath();
            else onlineSources.AdbPath = Properties.Settings.Default.AdbPath;

            SelectedSource = DetermineSource();

            onlineSources.Sources.CollectionChanged+=Sources_CollectionChanged;
        }

        private void Sources_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (null != currentSource || 0==onlineSources.Sources.Count) return;
            SelectedSource = onlineSources.DefaultSource();
        }

        void InitAdbPath()
        {
            Properties.Settings.Default.AdbPath = "";
            Properties.Settings.Default.Save();

            Dispatcher.BeginInvoke((Action)(() => ConfigureCmd(null, null)));
        }

        Sources.ISource DetermineSource()
        {
            if (Environment.GetCommandLineArgs().Length > 1) 
                return new Sources.AdbLogcatFile(Environment.GetCommandLineArgs()[1]);
            else return onlineSources.DefaultSource();
        }

        void NewSource(Sources.ISource src)
        {
            if (null != currentSource)
            {
                currentSource.Close();
                ClearData();
            }

            currentSource = src;

            // очистка выделения закрытого источника как открытого
            cleaning = true;
            OnPropertyChanged("OnlineSources");
            cleaning = false;
            OnPropertyChanged("OnlineSources"); 

            if (null == src)
            {
                TitleAddition = null;
                return;
            }

            if (src.Open())
            {
                TitleAddition = src.ShortName;

                logVM.IsLimited = !src.IsLimited;
                plotsVM.IsLimited = !src.IsLimited;

                Thread parse = src.IsLimited ? OfflineThread(src) : OnlineThread(src);
                try
                {
                    parse.Start();
                    src.Start();
                }
                catch
                {
                    src.Close();
                    TitleAddition = "reading error";
                }
            }
            else
            {
                TitleAddition = "source error";
                currentSource = null;
            }
        }

        void ClearData()
        {
            plotsVM.Clear();
            logVM.Clear();
        }

        Thread OnlineThread(Sources.ISource src)
        {
            var syncer = sync.NewLimitedSync(maxLimit, maxLimit); //TODO on limits change

            return new Thread(() =>
            {
                try
                {
                    Parse(src, 
                        (sample, dt) => Dispatcher.BeginInvoke((Action)(() =>
                            plotsVM.NewSampleOnline(NextSampleIdx(dt, syncer), sample))),
                        (record) => Dispatcher.Invoke((Action)(() =>
                        {
                            logVM.NewLineOnline(syncer.NextLineIdx(record.datetime), record.datetime.ToLongTimeString() + " " + record.content);
                            if (logVM.IsShowNewest) ScrollLog();
                        }))
                    );
                }
                catch { }

                Dispatcher.Invoke((Action)ClearData);

            }) { IsBackground = true, Name = "Next Line "+src.ShortName };
        }

        Thread OfflineThread(Sources.ISource src)
        {
            var syncer = sync.NewUnlimitedSync();

            return new Thread(() =>
            {
                Parse(src,
                    (sample, dt) => Dispatcher.Invoke((Action)(() =>
                        plotsVM.NewSampleOffline(NextSampleIdx(dt, syncer), sample))),
                    (record) => Dispatcher.Invoke((Action)(() =>
                        logVM.NewLineOnline(syncer.NextLineIdx(record.datetime), record.datetime.ToLongTimeString()+" "+record.content)))
                );

                Dispatcher.Invoke((Action)(() =>
                {
                    UpdateLog();
                    plotsVM.Offlined();
                }));

            }) { IsBackground = true };
        }

        int NextSampleIdx(DateTime dt, Features.Synchronization.SamplesLinesSync.ISynchronizator syncer)
        {
            int sampleIdx = syncer.NextSampleIdx(dt);
            logsPlotter(sampleIdx, syncer.LineIdx(sampleIdx).Count);

            return sampleIdx;
        }

        static void Parse(Sources.ISource src, Unity.IPSampleFilter.SampleReady sampleReadyCB, Action<Adb.LogRecord> newLineCB)
        {
            Unity.IPSampleFilter filter = new Unity.IPSampleFilter();
            filter.SampleReadyEvent += sampleReadyCB;

            Adb.Parser.Parse(src.NextLine,
                (record) =>
                {
                    Utils.Relay<Adb.LogRecord>.Race(record)(filter.Filter)(Utils.Relay<Adb.LogRecord>.ToParty(newLineCB));
                }
            );
        }

        void ScrollTo(int idx, int count)
        {
            ScrollLog(idx*100.0/count);
        }

        void ScrollLog(double to=100)
        {
            IScrollProvider scrollInterface = (IScrollProvider)svAutomation.GetPattern(PatternInterface.Scroll);
            if (null != scrollInterface && scrollInterface.VerticallyScrollable)
            {
#if !DEBUG
                try
                {
#endif
                to = to > 100 ? 100 : to;
                to = to < 0 ? 0 : to;

                scrollInterface.SetScrollPercent(-1, to);
                if(100==to) scrollInterface.Scroll(ScrollAmount.NoAmount, ScrollAmount.LargeIncrement);
#if !DEBUG
                }
                catch {}
#endif
            }
        }

        void UpdateLog()
        {
            if (logVM.IsShowNewest) ScrollLog();
        }

        public string TitleAddition
        {
            get
            {
                if (null != titleAddition) return "- " + titleAddition;
                else return "";
            }
            set { titleAddition = value; OnPropertyChanged("TitleAddition"); }
        }

        public bool IsSynced
        {
            get { return sync.IsSynced; }
            set { sync.IsSynced = value; OnPropertyChanged("IsSynced"); }
        }

        public bool IsSliderVisible
        {
            get { return isSliderVisible; }
            set { isSliderVisible = value; OnPropertyChanged("IsSliderVisible"); }
        }

        public ObservableCollection<Ula.Sources.ISource> OnlineSources
        {
            get { if (cleaning) return null; else return onlineSources.Sources; }
        }

        public Sources.ISource SelectedSource
        {
            get { return currentSource; }
            set { if(null != value) NewSource(value); OnPropertyChanged("SelectedSource"); }
        }

        #region utils
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private void Log_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateLog();
        }

        #region SliderPanel

        private void Slider_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            sliderVisibilityTimer.Stop();
        }

        private void Slider_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            sliderVisibilityTimer.Start();
        }

        private void UserFroze(object sender, RoutedEventArgs e)
        {
            sliderVisibilityTimer.Stop();
            IsSliderVisible = true;
        }

        private void Slider_MouseLeftButtonDown(object sender, System.Windows.Input.MouseEventArgs e)
        {
            sliderVisibilityTimer.Stop();
            IsSliderVisible = false;
        }
        #endregion

        private void OpenFileCmd(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            if (dlg.ShowDialog() == true) SelectedSource =new Sources.AdbLogcatFile(dlg.FileName);
        }

        private void ToggleAllAutoCmd(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            if (!plotsVM.IsLimited) return;

            if (plotsVM.IsShowNewest != logVM.IsShowNewest)
            {
                logVM.IsShowNewest = plotsVM.IsShowNewest = false;
            }
            else
            {
                logVM.IsShowNewest = !logVM.IsShowNewest;
                plotsVM.IsShowNewest = logVM.IsShowNewest;
            }

            ToggleLogAutoCmd(sender, e);
        }

        private void ToggleLogAutoCmd(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            UpdateLog();
        }

        private void ConfigureCmd(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            UI.ConfigDialog config = new UI.ConfigDialog(Properties.Settings.Default.AdbPath) { Owner = this };

            if(true==config.ShowDialog())
            {
                Properties.Settings.Default.AdbPath = config.AdbPath;
                Properties.Settings.Default.Save();

                onlineSources.AdbPath = config.AdbPath;
            }
        }
    }
}
