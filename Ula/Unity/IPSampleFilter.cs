﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Ula.Unity
{
    public class IPSample{
        public struct Stat{
            public double min;
            public double max;
            public double avg;
        }

        public struct StatBatched{
            public int min;
            public int max;
            public int avg;
            public int batched;
        }

        public struct PlayerDetail{
            public double physx;
            public double animation;
            public double culling;
            public double skinning;
            public double batching;
            public double render;
            public int fixed_update_count_min;
            public int fixed_update_count_max;
        }

        public struct MonoScripts{
            public double update;
            public double fixedUpdate;
            public double coroutines;
        }
        
        public struct MonoMemory{
            public int used_heap;
            public int allocated_heap;
            public int max_number_of_collections;
            public double collection_total_duration;
        }

        public Stat cpu_player;
        public Stat cpu_ogles_drv;
        public Stat cpu_waits_gpu;
        public Stat msaa_resolve;
        public Stat cpu_present;
        public Stat frametime;
        public StatBatched draw_call;
        public StatBatched tris;
        public StatBatched verts;
        public PlayerDetail player_detail;
        public MonoScripts mono_scripts; 
        public MonoMemory mono_memory; 
        }

    public class IPSampleFilter
    {
        public delegate void SampleReady(IPSample sample, DateTime dt);

        public event SampleReady SampleReadyEvent = (s, t) => { };
        public event Action<string> UnknownParameterEvent = (s) => { };

        static readonly string startStr = "Android Unity internal profiler stats:";
        static readonly string stopStr = "----------------------------------------";
        static readonly char[] statNameSeparator = new char[] { '>' };

        Func<string,string> state;

        Func<string, string> waitNewSample;
        Func<string, string> readSample;

        DateTime currDT;

        public IPSampleFilter()
        {
            IPSample sample=null;

            waitNewSample = (s) =>
            {
                if ( startStr == s)
                {
                    sample = new IPSample();
                    state = readSample;
                    return null;
                }
                else return s;
            };

            readSample = (s) =>
            {
                if (stopStr == s)
                {
                    if(null!=sample) OnSampleReadyEvent(sample);
                    Reset();
                }
                else if (startStr == s)  // skip partial sample
                {
                    sample = new IPSample(); 
                }
                else
                {
                    string[] statName = s.Split(statNameSeparator, 2);
                    if (2 == statName.Length && statName[0].Length > 0)
                    {
                        if (!Parse(ref sample, statName[0], statName[1])) UnknownParameterEvent(s);
                    }
                    else Reset();
                }

                return null;
            };

            Reset();
        }

        void OnSampleReadyEvent(IPSample sample)
        {
 	        SampleReadyEvent(sample, currDT);
        }

        public Adb.LogRecord Filter(Adb.LogRecord parsed)
        {
            currDT = parsed.datetime;

            if (null == parsed.content)
            {
                Reset();
                return null;
            }
            
            parsed.content = state(parsed.content);
            if (null == parsed.content) return null;
            return parsed;
        }

        void Reset()
        {
            state = waitNewSample;
        }

        delegate bool Parser(IPSample sample, string original);

        static Dictionary<string, Parser> parsers = new Dictionary<string, Parser>()
        {
            {"cpu-player",   (sample, original) => ParseStat(original,(stat)=>sample.cpu_player=stat)},
            {"cpu-ogles-drv",(sample, original) => ParseStat(original,(stat)=>sample.cpu_ogles_drv=stat)},
            {"cpu-waits-gpu",(sample, original) => ParseStat(original,(stat)=>sample.cpu_waits_gpu=stat)},
            {"msaa-resolve", (sample, original) => ParseStat(original,(stat)=>sample.msaa_resolve=stat)},
            {"cpu-present",  (sample, original) => ParseStat(original,(stat)=>sample.cpu_present=stat)},
            {"frametime",    (sample, original) => ParseStat(original,(stat)=>sample.frametime=stat)},

            {"draw-call #",(sample, original) => ParseStatBatched(original,(stat)=>sample.draw_call=stat)},
            {"tris #",     (sample, original) => ParseStatBatched(original,(stat)=>sample.tris=stat)},
            {"verts #",    (sample, original) => ParseStatBatched(original,(stat)=>sample.verts=stat)},

            {"player-detail",(sample, original) => ParsePlayerDetail(original,(stat)=>sample.player_detail=stat)},
            {"mono-scripts", (sample, original) => ParseMonoScripts(original,(stat)=>sample.mono_scripts=stat)},
            {"mono-memory",  (sample, original) => ParseMonoMemory(original,(stat)=>sample.mono_memory=stat)},
        };

        static bool Parse(ref IPSample sample, string name, string original)
        {
            Parser p;
            if (parsers.TryGetValue(name, out p)) return p(sample, original);
            else return false;
        }
        
        static readonly System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-EN");

        static double ParseDouble(string str)
        {
            return double.Parse(str, System.Globalization.NumberStyles.Float, culture);
        }

        // min:  0.3   max:  1.6   avg:  0.5
        static readonly Regex StatRegex = new Regex(@".*min:\s*(-?\d+\.?\d+)\s*max:\s*(-?\d+\.?\d+)\s*avg:\s*(-?\d+\.?\d+)", RegexOptions.Singleline | RegexOptions.Compiled);

        static bool ParseStat(string original, Action<IPSample.Stat> put)
        {
            Match match =StatRegex.Match(original);
            if (! match.Success || 4 != match.Groups.Count) return false;

            try
            {
                put(new IPSample.Stat()
                {
                    min = ParseDouble(match.Groups[1].Value),
                    max = ParseDouble(match.Groups[2].Value),
                    avg = ParseDouble(match.Groups[3].Value)
                });
            }
            catch
            {
                return false;
            }

            return true;
        }

        // min:  13    max:  15    avg:  14     | batched:    80
        static readonly Regex StatBatchedRegex = new Regex(@".*min:\s*(\d+)\s*max:\s*(\d+)\s*avg:\s*(\d+)\s*\|\s*batched:\s*(\d+)", RegexOptions.Singleline | RegexOptions.Compiled);

        static bool ParseStatBatched(string original, Action<IPSample.StatBatched> put)
        {
            Match match = StatBatchedRegex.Match(original);
            if (!match.Success || 5 != match.Groups.Count) return false;

            try
            {
                put(new IPSample.StatBatched()
                {
                    min = int.Parse(match.Groups[1].Value),
                    max = int.Parse(match.Groups[2].Value),
                    avg = int.Parse(match.Groups[3].Value),
                    batched = int.Parse(match.Groups[4].Value)
                });
            }
            catch
            {
                return false;
            }

            return true;
        }

        // physx:  0.4 animation:  1.3 culling  0.0 skinning:  0.0 batching:  0.5 render: 11.2 fixed-update-count: 0 .. 2
        static readonly Regex PlayerDetailRegex =
            new Regex(@".*physx:\s*(\d+\.?\d+)\s*animation:\s*(\d+\.?\d+)\s*culling:?\s*(\d+\.?\d+)\s*skinning:\s*(\d+\.?\d+)\s*batching:\s*(\d+\.?\d+)\s*render:\s*(\d+\.?\d+)\s*fixed-update-count:\s*(\d+)\s*\.\.\s*(\d+)", RegexOptions.Singleline | RegexOptions.Compiled);

        static bool ParsePlayerDetail(string original, Action<IPSample.PlayerDetail> put)
        {
            Match match = PlayerDetailRegex.Match(original);
            if (!match.Success || 9 != match.Groups.Count) return false;

            try
            {
                put(new IPSample.PlayerDetail()
                {
                    physx =     ParseDouble(match.Groups[1].Value),
                    animation = ParseDouble(match.Groups[2].Value),
                    culling =   ParseDouble(match.Groups[3].Value),
                    skinning =  ParseDouble(match.Groups[4].Value),
                    batching =  ParseDouble(match.Groups[5].Value),
                    render =    ParseDouble(match.Groups[6].Value),
                    fixed_update_count_min = int.Parse(match.Groups[7].Value),
                    fixed_update_count_max = int.Parse(match.Groups[8].Value),
                });
            }
            catch
            {
                return false;
            }

            return true;
        }

        // update:  0.5   fixedUpdate:  0.1 coroutines:  0.0 
        static readonly Regex MonoScriptsRegex = new Regex(@".*update:\s*(\d+\.?\d+)\s*fixedUpdate:\s*(\d+\.?\d+)\s*coroutines:\s*(\d+\.?\d+)", RegexOptions.Singleline | RegexOptions.Compiled);

        static bool ParseMonoScripts(string original, Action<IPSample.MonoScripts> put)
        {
            Match match = MonoScriptsRegex.Match(original);
            if (!match.Success || 4 != match.Groups.Count) return false;

            try
            {
                put(new IPSample.MonoScripts()
                {
                    update =      ParseDouble(match.Groups[1].Value),
                    fixedUpdate = ParseDouble(match.Groups[2].Value),
                    coroutines =  ParseDouble(match.Groups[3].Value)
                });
            }
            catch
            {
                return false;
            }

            return true;
        }

        // used heap: 393216 allocated heap: 479232  max number of collections: 0 collection total duration:  0.0
        static readonly Regex MonoMemoryRegex = new Regex(@".*used heap:\s*(\d+)\s*allocated heap:\s*(\d+)\s*max number of collections:\s*(\d+)\s*collection total duration:\s*(\d+\.?\d+)", RegexOptions.Singleline | RegexOptions.Compiled);

        static bool ParseMonoMemory(string original, Action<IPSample.MonoMemory> put)
        {
            Match match = MonoMemoryRegex.Match(original);
            if (!match.Success || 5 != match.Groups.Count) return false;

            try
            {
                put(new IPSample.MonoMemory()
                {
                    used_heap =                 int.Parse(match.Groups[1].Value),
                    allocated_heap=             int.Parse(match.Groups[2].Value),
                    max_number_of_collections = int.Parse(match.Groups[3].Value),
                    collection_total_duration = ParseDouble(match.Groups[4].Value)
                });
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
