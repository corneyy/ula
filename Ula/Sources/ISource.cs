﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ula.Sources
{
    public interface ISource
    {
        bool Open();
        void Start();
        void Close();
        string NextLine();
        string ShortName{get;}

        /// <summary>
        /// Количество данных в источнике
        /// </summary>
        bool IsLimited { get; }
        bool IsOpen { get; }
    }
}
