﻿using Managed.Adb;
using Managed.Adb.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Threading;

namespace Ula.Sources
{
    public class AdbLogcatOnlineSourceProvider
    {
        const int bufferLength = 10;
        string logcatCmd = "logcat -v time -s Unity";
        ObservableCollection<ISource> sources = new ObservableCollection<ISource>();
        IPEndPoint adbAddress = AndroidDebugBridge.SocketAddress;
        public static Device nullDevice = new Device("bad", new DeviceState(), "bad", "bad", "bad");
        AdbLogcatOnline currentSource;
        volatile bool isNeedRepair=false;
        AdbLogcatOnline repairSource;
        DispatcherTimer repairTimer;
        Dispatcher Dispatcher;

        public AdbLogcatOnlineSourceProvider(Dispatcher Dispatcher)
        {
            this.Dispatcher = Dispatcher;

            repairTimer = new DispatcherTimer();
            repairTimer.Interval = TimeSpan.FromSeconds(1);
            repairTimer.Tick += delegate
            {
                if (isNeedRepair)
                {
                    if (null == currentSource || !currentSource.isOpen || repairSource != currentSource)
                        isNeedRepair = false;
                    else if (Open())
                    {
                        isNeedRepair = false;
                        currentSource.Start();
                    }
                }
            };

            repairTimer.Start();

            Thread deviceTracking = new Thread(() =>
            {
                byte[] buffer = new byte[1024];
                while (true)
                {
                    try
                    {
                        Socket adbChan = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                        adbChan.Connect(adbAddress);

                        if (SendDeviceListMonitoringRequest(adbChan))
                        {
                            while(0 != adbChan.Receive(buffer, 1, SocketFlags.None))
                            {
                               while (0 != adbChan.Available)
                               {
                                   if (adbChan.Available > buffer.Length) adbChan.Receive(buffer);
                                   else adbChan.Receive(buffer, adbChan.Available, SocketFlags.None);
                               }

                               List<Device> devices = AdbHelper.Instance.GetDevices(AndroidDebugBridge.SocketAddress);

                               Dispatcher.Invoke((Action)(() => UpdateSources(devices)));
                            }
                        }
                    }
                    catch { }

                    Thread.Sleep(1000);
                }
            }) { IsBackground = true, Name = "Devices tracking" };

            deviceTracking.Start();
        }

        public string AdbPath
        {
            set
            {
                try
                {
                    if (System.IO.File.Exists(value)) 
                    {
                        System.Diagnostics.Process cmd = new System.Diagnostics.Process();
                        cmd.StartInfo.FileName = value;
                        cmd.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        cmd.StartInfo.Arguments = "start-server";

                        cmd.Start();
                    }
                }
                catch { }
            }
        }

        public ObservableCollection<ISource> Sources
        {
            get { return sources;}
        }

        public ISource DefaultSource()
        {
            if (sources.Count > 0) return sources[0];
            return new AdbLogcatOnline(this, nullDevice);
        }

        void UpdateSources(List<Device> devices)
        {
            lock (sources)
            {
                try
                {
                    sources.Clear();

                    devices.ForEach((d) =>
                    {
                        if (null != currentSource && d.SerialNumber == currentSource.Device.SerialNumber)
                            sources.Add(currentSource);
                        else sources.Add(new AdbLogcatOnline(this, d));
                    });
                }
                catch
                {
                    sources.Clear();
                }
            }
        }

        bool Open(AdbLogcatOnline source)
        {
            currentSource = source;
            return Open();
        }

        bool Open()
        {
            if (null == currentSource) return false;

            if (!sources.Contains(currentSource)) return false;

            isNeedRepair = false;
            currentSource.isOpen = true;
            return true;
        }

        void RemoteCommandIsDown()
        {
            repairSource = currentSource;
            isNeedRepair = null==currentSource?false:currentSource.isOpen;
        }

        void Close(AdbLogcatOnline source)
        {
            if (currentSource == source) currentSource = null;
        }

        void ExecuteRemoteCommand(AdbLogcatOnline source, IShellOutputReceiver rcvr)
        {
            ExecuteRemoteCommand(AndroidDebugBridge.SocketAddress, logcatCmd, source, rcvr);
        }

        // from https://madb.codeplex.com/SourceControl/latest#trunk/Managed.AndroidDebugBridge/Managed.AndroidDebugBridge/AdbHelper.cs
        static void ExecuteRemoteCommand(IPEndPoint endPoint, String command, AdbLogcatOnline source, IShellOutputReceiver rcvr, int maxTimeToOutputResponse = int.MaxValue)
        {
            using ( var socket = ExecuteRawSocketCommand(endPoint, source.Device, AdbHelper.Instance.FormAdbRequest("shell:{0}".With(command))))
            {
                if (StopThread()) return;

                socket.ReceiveTimeout = maxTimeToOutputResponse;
                socket.SendTimeout = maxTimeToOutputResponse;
                byte[] data = new byte[16384];
                int count = -1;
                while (count != 0)
                {
                    if (rcvr.IsCancelled) throw new OperationCanceledException();

                    count = socket.Receive(data);
                    
                    if (StopThread()) return;

                    if (count != 0) rcvr.AddOutput(data, 0, count);
                }
            }
        }

        static Socket ExecuteRawSocketCommand(IPEndPoint address, Device device, byte[] command)
        {
            if (device != null && !device.IsOnline)
            {
                throw new AdbException("Device is offline");
            }

            Socket adbChan = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            adbChan.Connect(address);
            
            if (StopThread()) return adbChan;

            if (device != null)
            {
                AdbHelper.Instance.SetDevice(adbChan, device);
            }
            if (!AdbHelper.Instance.Write(adbChan, command))
            {
                throw new AdbException("failed to submit the command: {0}.".With(command.GetString().Trim()));
            }

            AdbResponse resp = AdbHelper.Instance.ReadAdbResponse(adbChan, false /* readDiagString */);
            if (!resp.IOSuccess || !resp.Okay)
            {
                throw new AdbException("Device rejected command: {0}".With(resp.Message));
            }
            return adbChan;
        }

        bool SendDeviceListMonitoringRequest(Socket adbChan)
        {
            if (AdbHelper.Instance.Write(adbChan, AdbHelper.Instance.FormAdbRequest("host:track-devices")) == false)
            {
                adbChan.Close();
                return false;
            }

            AdbResponse resp = AdbHelper.Instance.ReadAdbResponse(adbChan, false);

            if (!resp.IOSuccess)
            {
                adbChan.Close();
                return false;
            }

            return resp.Okay;
        }

        static bool StopThread()
        {
            return (Thread.CurrentThread.ThreadState & ThreadState.AbortRequested) != 0; 
        }

        // from https://madb.codeplex.com/SourceControl/latest#trunk/Managed.AndroidDebugBridge/Managed.AndroidDebugBridge/Receivers/MultilineReceiver.cs
        class MultiLineReceiver : Managed.Adb.IShellOutputReceiver
        {

            protected const String NEWLINE = "\r\n";
            protected const String ENCODING = "ISO-8859-1";

            readonly int bufferLength;

            public bool TrimLines { get; set; }

            protected String UnfinishedLine { get; set; }

            Queue<string> buffer;
            public MultiLineReceiver(Queue<string> buffer, int bufferLength)
            {
                this.buffer = buffer;
                this.bufferLength = bufferLength;
            }

            public void AddOutput(byte[] data, int offset, int length)
            {
                if (!IsCancelled)
                {
                    String s = null;
                    try
                    {
                        s = Encoding.GetEncoding(ENCODING).GetString(data, offset, length); //$NON-NLS-1$
                    }
                    catch (DecoderFallbackException)
                    {
                        // normal encoding didn't work, try the default one
                        s = Encoding.Default.GetString(data, offset, length);
                    }

                    // ok we've got a string
                    if (!String.IsNullOrEmpty(s))
                    {
                        // if we had an unfinished line we add it.
                        if (!String.IsNullOrEmpty(UnfinishedLine))
                        {
                            s = UnfinishedLine + s;
                            UnfinishedLine = null;
                        }

                        // now we split the lines
                        //Lines.Clear ( );
                        int start = 0;
                        do
                        {
                            int index = s.IndexOf(NEWLINE, start); //$NON-NLS-1$

                            // if \r\n was not found, this is an unfinished line
                            // and we store it to be processed for the next packet
                            if (index == -1)
                            {
                                UnfinishedLine = s.Substring(start);
                                break;
                            }

                            // so we found a \r\n;
                            // extract the line
                            String line = s.Substring(start, index - start);
                            if (TrimLines)
                            {
                                line = line.Trim();
                            }

                            lock (buffer)
                            {
                                if (buffer.Count() >= bufferLength)
                                {
                                    if (StopThread()) return;
                                    Monitor.Wait(buffer);
                                }
                                buffer.Enqueue(line);
                                Monitor.Pulse(buffer);
                            }
                            // move start to after the \r\n we found
                            start = index + 2;
                        } while (true);
                    }
                }
            }

            public void Flush()
            {
                lock (buffer)
                {
                    buffer.Clear();
                }
            }

            public virtual bool IsCancelled { get; protected set; }
        }

        protected class AdbLogcatOnline : ISource
        {
            public volatile Queue<string> buffer = new Queue<string>(bufferLength);
            public volatile bool isOpen = false;

            AdbLogcatOnlineSourceProvider provider;
            Thread readingThread;
            
            public AdbLogcatOnline(AdbLogcatOnlineSourceProvider provider, Device device)
            {
                this.provider = provider;
                Device = device;
            }

            public Device Device
            {
                get;
                protected set;
            }

            #region ISource

            public bool Open()
            {
                lock (buffer)
                {
                    buffer.Clear();
                    Monitor.PulseAll(buffer);
                }

                return provider.Open(this);
            }

            public void Start()
            {
                if (!isOpen || Device == nullDevice) return;

                Thread prevThread=null;

                if (null != readingThread && readingThread.IsAlive)
                {
                    prevThread = readingThread;

                    try
                    {
                        prevThread.Abort();
                        prevThread.Interrupt();
                    }
                    catch { }
                }

                MultiLineReceiver reciever = new MultiLineReceiver(buffer, bufferLength);
                readingThread = new Thread(() =>
                {
                    try
                    {
                        if (null != prevThread) prevThread.Join();

                        provider.ExecuteRemoteCommand(this, reciever);
                    }
                    catch(ThreadAbortException)
                    {
                        return;
                    }
                    catch (ThreadInterruptedException)
                    {
                        return;
                    }
                    catch { }
                    {

                    }

                    if (!StopThread()) provider.RemoteCommandIsDown();

                }) { IsBackground = true, Name = Device.SerialNumber };

                readingThread.Start();
            }

            public string NextLine()
            {
                if (!isOpen) return null;

                string s;
                lock (buffer)
                {
                    while (0 == buffer.Count) Monitor.Wait(buffer);

                    s = buffer.Dequeue();
                    Monitor.Pulse(buffer);
                }
                return s;
            }

            public void Close()
            {
                isOpen = false;

                try
                {
                    if (null != readingThread)
                    {
                        readingThread.Abort();
                        readingThread.Interrupt();
                    }
                }
                catch { }

                lock (buffer)
                {
                    buffer.Clear();
                    buffer.Enqueue(null);
                    Monitor.PulseAll(buffer);
                }

                provider.Close(this);
            }

            public string ShortName
            {
                get { return Device.SerialNumber; }
            }

            public bool IsLimited
            {
                get { return false; }
            }

            public bool IsOpen
            {
                get { return isOpen; }
            }
            #endregion
        }
    }
}
