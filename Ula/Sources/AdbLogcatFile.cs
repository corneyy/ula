﻿using Managed.Adb;
using Managed.Adb.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Ula.Sources
{
    class AdbLogcatFile: ISource
    {
        string fileName;
        System.IO.StreamReader reader;

        public AdbLogcatFile(string fileName)
        {
            this.fileName = fileName;
        }

        public bool Open()
        {
            try
            {
                reader = new System.IO.StreamReader(System.IO.File.OpenRead(fileName));
                ShortName = System.IO.Path.GetFileName(fileName);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public void Start(){}

        public string NextLine()
        {
            try
            {
                return (null == reader || reader.EndOfStream) ? null : reader.ReadLine();
            }
            catch
            {
                return null;
            }
        }

        public void Close()
        {
            if (null == reader) return;
            reader.Close();
            reader = null;
        }

        public string ShortName
        {
            get;
            protected set;
        }

        public bool IsLimited
        {
            get { return true; }
        }

        public bool IsOpen
        {
            get { return null == reader; }
        }
    }
}
