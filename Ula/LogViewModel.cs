﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using System.Collections.ObjectModel;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;

namespace Ula
{
    class LogViewModel : INotifyPropertyChanged, Features.IMultiSelectable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public class Line : Utils.NotifyPropertyChanged
        {
            public int idx;
            public string line;
            public bool selected;

            public Line( string line)
            {
                this.idx = -1;
                this.line = line;
            }

            public Line(int idx, string line)
            {
                this.idx = idx;
                this.line = line;
            }

            public string Text
            {
                get {return line;}
            }
            public bool IsSelected
            {
                get { return selected; }
                set { selected = value; OnPropertyChanged("IsSelected"); }
            }
        }

        int limit = 1000;
        bool isLimited = false;
        Utils.IObservableCollection<Line> lines = new Utils.ObservableCollectionIfaced<Line>();
        bool isShowNewest = true;
        
        Action<int, int> showLineCB;

        public LogViewModel(Action<int, int> showLineCB)
        {
            this.showLineCB = showLineCB;
        }
        
        public void NewLineOnline(string line)
        {
            lines.Add(new Line(line));
        }

        public void NewLineOnline(int idx, string line)
        {
            lines.Add(new Line(idx, line));
        }

        public void Clear()
        {
            lines.Clear();
        }

        public Utils.IObservableCollection<Line> Lines
        {
            get { return lines; }
            protected set { lines = value; OnPropertyChanged("Lines"); }
        }

        public bool IsShowNewest
        {
            get { return isShowNewest; }
            set { isShowNewest = value; OnPropertyChanged("IsShowNewest"); }
        }

        /// <summary>
        /// Ограничено ли количество хранимых данных
        /// </summary>
        public bool IsLimited
        {
            get { return isLimited; }
            set
            { 
                if(value != isLimited)
                {
                    if (value) Lines = new Utils.ObservableRingBuffer<Line>(limit);
                    else Lines = new Utils.ObservableCollectionIfaced<Line>();

                    isLimited = value;
                    OnPropertyChanged("IsLimited"); 
                }
            }
        }

        public int Limit
        {
            get { return limit; }
            set
            {
                if(value != limit)
                { 
                    if (isLimited) Lines = new Utils.ObservableRingBuffer<Line>(value); //TODO Expand/Trim current RB

                    limit = value;
                    OnPropertyChanged("Limit");
                }
            }
        }

        public Line SelectedItem
        {
            get
            {
                return null;
            }
            set
            {
                Selected(value.idx);
            }
        }

        #region Features.ISelectable

        void Features.IMultiSelectable.Select(IList<int> list)
        {
            int lastSelectedIdx = -1;
            int count = 0;

            for (int i=0; i < lines.Count; i++)
            {
                if (list.Contains(lines[i].idx))
                {
                    lines[i].IsSelected = true;
                    ++count;
                    lastSelectedIdx = i;
                }
                else lines[i].IsSelected = false;
            }

            if (lastSelectedIdx>=0) showLineCB(lastSelectedIdx, lines.Count);
        }

        void Features.ISelectable.Unselect()
        {
            Unselect();
        }

        void Unselect()
        {
            lines.Where(l => l.selected).ToList().ForEach(l => l.IsSelected = false);
        }

        public event Action<int> Selected = (i) => { };
        #endregion

        #region utils

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
