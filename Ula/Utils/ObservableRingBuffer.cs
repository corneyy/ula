﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Ula.Utils
{
    interface IObservableCollection<T> : ICollection<T>, IList<T>, INotifyCollectionChanged, INotifyPropertyChanged { }

    public class ObservableCollectionIfaced<T> : ObservableCollection<T>, IObservableCollection<T> { }

    //from http://www.e-pedro.com/2009/04/creating-a-custom-observable-collection-in-wpf/

    public class ObservableRingBuffer<T> : IObservableCollection<T>
    {
        const string CountString = "Count";
        const string IndexerName = "Item[]";
        readonly SimpleMonitor monitor;
 
        RingBuffer<T> ring;

        public ObservableRingBuffer(int capacity)
        {
            ring = new RingBuffer<T>(capacity);
            this.monitor = new SimpleMonitor();
        }

        #region ICollection

        public void Add(T item)
        {
            CheckReentrancy();
            ring.Add(item);
            OnPropertyChanged(CountString);
            OnPropertyChanged(IndexerName);
            if (ring.Count < ring.Capacity) OnCollectionChanged(NotifyCollectionChangedAction.Add, item, ring.Count);
            else OnCollectionChanged(NotifyCollectionChangedAction.Reset, null);
        }

        public void Clear()
        {
            CheckReentrancy();
            ring.Clear();
            OnPropertyChanged(CountString);
            OnPropertyChanged(IndexerName);
            OnCollectionReset();
        }

        public bool Contains(T item)
        {
            throw new System.NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new System.NotImplementedException();
        }

        public int Count
        {
            get { return ring.Count; }
        }

        public bool IsReadOnly
        {
            get { return ((ICollection<T>)ring).IsReadOnly; }
        }

        public bool Remove(T item)
        {
            //CheckReentrancy();
            throw new System.NotImplementedException();
//            OnPropertyChanged(CountString);
//            OnPropertyChanged(IndexerName);
//            OnCollectionChanged(NotifyCollectionChangedAction.Remove, item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ring.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return ring.GetEnumerator();
        }
        #endregion

        #region IList

        public int IndexOf(T item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, T item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public T this[int index]
        {
            get { return ring[index]; }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        #region INotifyCollectionChanged

        public event NotifyCollectionChangedEventHandler CollectionChanged;
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (CollectionChanged != null)
            {
                using (BlockReentrancy())
                {
                    CollectionChanged(this, e);
                }
            }
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, object item)
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(action, item));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, object item, int index)
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(action, item, index));
        }

        private void OnCollectionReset()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region SimpleMonitor
        protected IDisposable BlockReentrancy()
        {
            this.monitor.Enter();
            return this.monitor;
        }

        protected void CheckReentrancy()
        {
            if ((this.monitor.Busy && (CollectionChanged != null)) && (CollectionChanged.GetInvocationList().Length > 1))
            {
                throw new InvalidOperationException("Collection Reentrancy Not Allowed");
            }
        }

        [Serializable]
        private class SimpleMonitor : IDisposable
        {
            private int busyCount;

            public bool Busy
            {
                get { return this.busyCount > 0; }
            }

            public void Enter()
            {
                this.busyCount++;
            }

            #region Implementation of IDisposable

            public void Dispose()
            {
                this.busyCount--;
            }

            #endregion
        }
        #endregion
    }
}
