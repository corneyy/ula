﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ula.Utils
{
    class Relay<T>
    {
        public delegate T Party(T s);
        public delegate Functor Functor(Party c);

        public static Functor Race(T baton)
        {
            return p => Pass(baton, p);
        }

        public static Party ToParty(Action<T> call)
        {
            return baton => { call(baton); return baton; };
        }

        static Functor Pass(T baton, Party p)
        {
            T ret = p(baton);
            if (null == ret) return Skip;
            return p1 => Pass(ret, p1);
        }

        static Functor Skip(Party p)
        {
            return Skip;
        }
    }
}
