﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ula.Utils
{
    static class Utils
    {
        static public double ToFPS(double ms)
        {
            if (0 == ms) return 0;
            return Math.Round(1000 / ms);
        }

        static public void ForEach(this Meta.Numerics.Statistics.HistogramBinsCollection list, Action<Meta.Numerics.Statistics.HistogramBin> action)
        {
            if (list == null) throw new ArgumentNullException("null");
            if (action == null) throw new ArgumentNullException("action");

            for (int i = 0; i < list.Count; i++)
            {
                action(list[i]);
            }
        }

        static public void ForEach<T>(this IList<T> list, Action<T> action)
        {
            if (list == null) throw new ArgumentNullException("null");
            if (action == null) throw new ArgumentNullException("action");

            for (int i = 0; i < list.Count; i++)
            {
                action(list[i]);
            }
        }
    }
}
