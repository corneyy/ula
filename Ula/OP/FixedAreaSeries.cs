﻿using OxyPlot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ula.OP
{

    // --------------------------------------------------------------------------------------------------------------------
    // <copyright file="AreaSeries.cs" company="OxyPlot">
    //   Copyright (c) 2014 OxyPlot contributors
    // </copyright>
    // <summary>
    //   Represents an area series that fills the polygon defined by two sets of points or one set of points and a constant.
    // </summary>
    // --------------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// Represents an area series that fills the polygon defined by two sets of points or one set of points and a constant.
    /// </summary>
    public class FixedAreaSeries : FixedLineSeries
    {
        /// <summary>
        /// The second list of points.
        /// </summary>
        private IList<DataPoint> points2;
        /// <summary>
        /// The secondary data points from the items source.
        /// </summary>
        private IList<DataPoint> itemsSourcePoints2 = new List<DataPoint>();

        /// <summary>
        /// Initializes a new instance of the <see cref = "AreaSeries" /> class.
        /// </summary>

        public FixedAreaSeries()
            : this(ListAllocator.instance) { }

        public FixedAreaSeries(IAllocator allocator)
            :base(allocator)
        {
            allocator.Alloc((dp) => { points2 = dp; });

            this.Reverse2 = true;
            this.Color2 = OxyColors.Automatic;
            this.Fill = OxyColors.Automatic;
        }

        /// <summary>
        /// Gets or sets a constant value for the area definition.
        /// This is used if DataFieldBase and BaselineValues are <c>null</c>.
        /// </summary>
        /// <value>The baseline.</value>
        public double ConstantY2 { get; set; }

        /// <summary>
        /// Gets or sets the second X data field.
        /// </summary>
        public string DataFieldX2 { get; set; }

        /// <summary>
        /// Gets or sets the second Y data field.
        /// </summary>
        public string DataFieldY2 { get; set; }

        /// <summary>
        /// Gets or sets the color of the second line.
        /// </summary>
        /// <value>The color.</value>
        public OxyColor Color2 { get; set; }

        /// <summary>
        /// Gets the actual color of the second line.
        /// </summary>
        /// <value>The actual color.</value>
        public OxyColor ActualColor2
        {
            get
            {
                return this.Color2.GetActualColor(this.ActualColor);
            }
        }

        /// <summary>
        /// Gets or sets the area fill color.
        /// </summary>
        /// <value>The fill.</value>
        public OxyColor Fill { get; set; }

        /// <summary>
        /// Gets the actual fill color.
        /// </summary>
        /// <value>The actual fill.</value>
        public OxyColor ActualFill
        {
            get
            {
                return this.Fill.GetActualColor(OxyColor.FromAColor(100, this.ActualColor));
            }
        }

        /// <summary>
        /// Gets the second list of points.
        /// </summary>
        /// <value>The second list of points.</value>
        public IList<DataPoint> Points2
        {
            get
            {
                return this.points2;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the second
        /// data collection should be reversed.
        /// The first dataset is not reversed, and normally
        /// the second dataset should be reversed to get a
        /// closed polygon.
        /// </summary>
        public bool Reverse2 { get; set; }

        /// <summary>
        /// Gets the second list of points.
        /// </summary>
        /// <value>The second list of points.</value>
        protected IList<DataPoint> ActualPoints2
        {
            get
            {
                return this.ItemsSource != null ? this.itemsSourcePoints2 : this.points2;
            }
        }

        /// <summary>
        /// Gets the nearest point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="interpolate">interpolate if set to <c>true</c> .</param>
        /// <returns>A TrackerHitResult for the current hit.</returns>
        public override TrackerHitResult GetNearestPoint(ScreenPoint point, bool interpolate)
        {
            TrackerHitResult result1, result2;
            if (interpolate && this.CanTrackerInterpolatePoints)
            {
                result1 = this.GetNearestInterpolatedPointInternal(this.ActualPoints, point);
                result2 = this.GetNearestInterpolatedPointInternal(this.ActualPoints2, point);
            }
            else
            {
                result1 = this.GetNearestPointInternal(this.ActualPoints, point);
                result2 = this.GetNearestPointInternal(this.ActualPoints2, point);
            }

            TrackerHitResult result;
            if (result1 != null && result2 != null)
            {
                double dist1 = result1.Position.DistanceTo(point);
                double dist2 = result2.Position.DistanceTo(point);
                result = dist1 < dist2 ? result1 : result2;
            }
            else
            {
                result = result1 ?? result2;
            }

            if (result != null)
            {
                result.Text = this.Format(
                    this.TrackerFormatString,
                    result.Item,
                    this.Title,
                    this.XAxis.Title ?? FixedXYAxisSeries.DefaultXAxisTitle,
                    this.XAxis.GetValue(result.DataPoint.X),
                    this.YAxis.Title ?? FixedXYAxisSeries.DefaultYAxisTitle,
                    this.YAxis.GetValue(result.DataPoint.Y));
            }

            return result;
        }

        /// <summary>
        /// Renders the series on the specified rendering context.
        /// </summary>
        /// <param name="rc">The rendering context.</param>
        /// <param name="model">The owner plot model.</param>
        public override void Render(IRenderContext rc, PlotModel model)
        {
            var actualPoints = this.ActualPoints;
            var actualPoints2 = this.ActualPoints2;
            int n0 = actualPoints.Count;
            if (n0 == 0)
            {
                return;
            }

            this.VerifyAxes();

            double minDistSquared = this.MinimumSegmentLength * this.MinimumSegmentLength;

            var clippingRect = this.GetClippingRect();
            rc.SetClip(clippingRect);

            // Transform all points to screen coordinates
            IList<ScreenPoint> pts0 = new ScreenPoint[n0];
            for (int i = 0; i < n0; i++)
            {
                pts0[i] = this.XAxis.Transform(actualPoints[i].X, actualPoints[i].Y, this.YAxis);
            }

            int n1 = actualPoints2.Count;
            IList<ScreenPoint> pts1 = new ScreenPoint[n1];
            for (int i = 0; i < n1; i++)
            {
                int j = this.Reverse2 ? n1 - 1 - i : i;
                pts1[j] = this.XAxis.Transform(actualPoints2[i].X, actualPoints2[i].Y, this.YAxis);
            }

            if (this.Smooth)
            {
                var rpts0 = ScreenPointHelper.ResamplePoints(pts0, this.MinimumSegmentLength);
                var rpts1 = ScreenPointHelper.ResamplePoints(pts1, this.MinimumSegmentLength);

                pts0 = CanonicalSplineHelper.CreateSpline(rpts0, 0.5, null, false, 0.25);
                pts1 = CanonicalSplineHelper.CreateSpline(rpts1, 0.5, null, false, 0.25);
            }

            var dashArray = this.ActualDashArray;

            // draw the clipped lines
            rc.DrawClippedLine(
                clippingRect,
                pts0,
                minDistSquared,
                this.GetSelectableColor(this.ActualColor),
                this.StrokeThickness,
                dashArray,
                this.LineJoin,
                false);
            rc.DrawClippedLine(
                clippingRect,
                pts1,
                minDistSquared,
                this.GetSelectableColor(this.ActualColor2),
                this.StrokeThickness,
                dashArray,
                this.LineJoin,
                false);

            // combine the two lines and draw the clipped area
            var pts = new List<ScreenPoint>();
            pts.AddRange(pts1);
            pts.AddRange(pts0);

            // pts = SutherlandHodgmanClipping.ClipPolygon(clippingRect, pts);
            rc.DrawClippedPolygon(clippingRect, pts, minDistSquared, this.GetSelectableFillColor(this.ActualFill), OxyColors.Undefined);

            var markerSizes = new[] { this.MarkerSize };

            // draw the markers on top
            rc.DrawMarkers(
                clippingRect,
                pts0,
                this.MarkerType,
                null,
                markerSizes,
                this.MarkerFill,
                this.MarkerStroke,
                this.MarkerStrokeThickness,
                1);
            rc.DrawMarkers(
                clippingRect,
                pts1,
                this.MarkerType,
                null,
                markerSizes,
                this.MarkerFill,
                this.MarkerStroke,
                this.MarkerStrokeThickness,
                1);

            rc.ResetClip();
        }

        /// <summary>
        /// Renders the legend symbol for the line series on the
        /// specified rendering context.
        /// </summary>
        /// <param name="rc">The rendering context.</param>
        /// <param name="legendBox">The bounding rectangle of the legend box.</param>
        public override void RenderLegend(IRenderContext rc, OxyRect legendBox)
        {
            double y0 = (legendBox.Top * 0.2) + (legendBox.Bottom * 0.8);
            double y1 = (legendBox.Top * 0.4) + (legendBox.Bottom * 0.6);
            double y2 = (legendBox.Top * 0.8) + (legendBox.Bottom * 0.2);

            var pts0 = new[] { new ScreenPoint(legendBox.Left, y0), new ScreenPoint(legendBox.Right, y0) };
            var pts1 = new[] { new ScreenPoint(legendBox.Right, y2), new ScreenPoint(legendBox.Left, y1) };
            var pts = new List<ScreenPoint>();
            pts.AddRange(pts0);
            pts.AddRange(pts1);
            rc.DrawLine(pts0, this.GetSelectableColor(this.ActualColor), this.StrokeThickness, this.ActualLineStyle.GetDashArray());
            rc.DrawLine(pts1, this.GetSelectableColor(this.ActualColor2), this.StrokeThickness, this.ActualLineStyle.GetDashArray());
            rc.DrawPolygon(pts, this.GetSelectableFillColor(this.ActualFill), OxyColors.Undefined);
        }

        /// <summary>
        /// The update data.
        /// </summary>
        protected override void UpdateData()
        {
            base.UpdateData();

            if (this.ItemsSource == null)
            {
                return;
            }

            throw new System.NotImplementedException();
/*
            this.itemsSourcePoints2.Clear();

          
            // Using reflection on DataFieldX2 and DataFieldY2

                        ReflectionExtensions.AddRange(this.itemsSourcePoints2, this.ItemsSource, this.DataFieldX2, this.DataFieldY2);
 */
        }

        /// <summary>
        /// Updates the maximum and minimum values of the series.
        /// </summary>
        protected override void UpdateMaxMin()
        {
            base.UpdateMaxMin();
            this.InternalUpdateMaxMin(this.ActualPoints2);
        }
    }

    // --------------------------------------------------------------------------------------------------------------------
    // <copyright file="CanonicalSplineHelper.cs" company="OxyPlot">
    //   Copyright (c) 2014 OxyPlot contributors
    // </copyright>
    // <summary>
    //   Provides functionality to interpolate a list of points by a canonical spline.
    // </summary>
    // --------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Provides functionality to interpolate a list of points by a canonical spline.
        /// </summary>
        /// <remarks>CanonicalSplineHelper.cs (c) 2009 by Charles Petzold (WPF and Silverlight)
        /// See also <a href="http://www.charlespetzold.com/blog/2009/01/Canonical-Splines-in-WPF-and-Silverlight.html">blog post</a>.</remarks>
        internal static class CanonicalSplineHelper
        {
            /// <summary>
            /// Creates a spline of data points.
            /// </summary>
            /// <param name="points">The points.</param>
            /// <param name="tension">The tension.</param>
            /// <param name="tensions">The tensions.</param>
            /// <param name="isClosed">True if the spline is closed.</param>
            /// <param name="tolerance">The tolerance.</param>
            /// <returns>A list of data points.</returns>
            internal static List<DataPoint> CreateSpline(IList<DataPoint> points, double tension, IList<double> tensions, bool isClosed, double tolerance)
            {
                var screenPoints = points.Select(p => new ScreenPoint(p.X, p.Y)).ToList();
                var interpolatedScreenPoints = CreateSpline(screenPoints, tension, tensions, isClosed, tolerance);
                var interpolatedDataPoints = new List<DataPoint>(interpolatedScreenPoints.Count);

                foreach (var s in interpolatedScreenPoints)
                {
                    interpolatedDataPoints.Add(new DataPoint(s.X, s.Y));
                }

                return interpolatedDataPoints;
            }

            /// <summary>
            /// Creates a spline of screen points.
            /// </summary>
            /// <param name="points">The points.</param>
            /// <param name="tension">The tension.</param>
            /// <param name="tensions">The tensions.</param>
            /// <param name="isClosed">True if the spline is closed.</param>
            /// <param name="tolerance">The tolerance.</param>
            /// <returns>A list of screen points.</returns>
            internal static List<ScreenPoint> CreateSpline(
                IList<ScreenPoint> points, double tension, IList<double> tensions, bool isClosed, double tolerance)
            {
                var result = new List<ScreenPoint>();
                if (points == null)
                {
                    return result;
                }

                int n = points.Count;
                if (n < 1)
                {
                    return result;
                }

                if (n < 2)
                {
                    result.AddRange(points);
                    return result;
                }

                if (n == 2)
                {
                    if (!isClosed)
                    {
                        Segment(result, points[0], points[0], points[1], points[1], tension, tension, tolerance);
                    }
                    else
                    {
                        Segment(result, points[1], points[0], points[1], points[0], tension, tension, tolerance);
                        Segment(result, points[0], points[1], points[0], points[1], tension, tension, tolerance);
                    }
                }
                else
                {
                    bool useTensionCollection = tensions != null && tensions.Count > 0;

                    for (int i = 0; i < n; i++)
                    {
                        double t1 = useTensionCollection ? tensions[i % tensions.Count] : tension;
                        double t2 = useTensionCollection ? tensions[(i + 1) % tensions.Count] : tension;

                        if (i == 0)
                        {
                            Segment(
                                result,
                                isClosed ? points[n - 1] : points[0],
                                points[0],
                                points[1],
                                points[2],
                                t1,
                                t2,
                                tolerance);
                        }
                        else if (i == n - 2)
                        {
                            Segment(
                                result,
                                points[i - 1],
                                points[i],
                                points[i + 1],
                                isClosed ? points[0] : points[i + 1],
                                t1,
                                t2,
                                tolerance);
                        }
                        else if (i == n - 1)
                        {
                            if (isClosed)
                            {
                                Segment(result, points[i - 1], points[i], points[0], points[1], t1, t2, tolerance);
                            }
                        }
                        else
                        {
                            Segment(result, points[i - 1], points[i], points[i + 1], points[i + 2], t1, t2, tolerance);
                        }
                    }
                }

                return result;
            }

            /// <summary>
            /// The segment.
            /// </summary>
            /// <param name="points">The points.</param>
            /// <param name="pt0">The pt 0.</param>
            /// <param name="pt1">The pt 1.</param>
            /// <param name="pt2">The pt 2.</param>
            /// <param name="pt3">The pt 3.</param>
            /// <param name="t1">The t 1.</param>
            /// <param name="t2">The t 2.</param>
            /// <param name="tolerance">The tolerance.</param>
            private static void Segment(
                IList<ScreenPoint> points,
                ScreenPoint pt0,
                ScreenPoint pt1,
                ScreenPoint pt2,
                ScreenPoint pt3,
                double t1,
                double t2,
                double tolerance)
            {
                // See Petzold, "Programming Microsoft Windows with C#", pages 645-646 or
                // Petzold, "Programming Microsoft Windows with Microsoft Visual Basic .NET", pages 638-639
                // for derivation of the following formulas:
                double sx1 = t1 * (pt2.X - pt0.X);
                double sy1 = t1 * (pt2.Y - pt0.Y);
                double sx2 = t2 * (pt3.X - pt1.X);
                double sy2 = t2 * (pt3.Y - pt1.Y);

                double ax = sx1 + sx2 + (2 * pt1.X) - (2 * pt2.X);
                double ay = sy1 + sy2 + (2 * pt1.Y) - (2 * pt2.Y);
                double bx = (-2 * sx1) - sx2 - (3 * pt1.X) + (3 * pt2.X);
                double by = (-2 * sy1) - sy2 - (3 * pt1.Y) + (3 * pt2.Y);

                double cx = sx1;
                double cy = sy1;
                double dx = pt1.X;
                double dy = pt1.Y;

                var num = (int)((Math.Abs(pt1.X - pt2.X) + Math.Abs(pt1.Y - pt2.Y)) / tolerance);

                // Notice begins at 1 so excludes the first point (which is just pt1)
                for (int i = 1; i < num; i++)
                {
                    double t = (double)i / (num - 1);
                    var pt = new ScreenPoint(
                        (ax * t * t * t) + (bx * t * t) + (cx * t) + dx,
                        (ay * t * t * t) + (by * t * t) + (cy * t) + dy);
                    points.Add(pt);
                }
            }
        }
}

