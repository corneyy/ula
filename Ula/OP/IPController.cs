﻿using OxyPlot;

namespace Ula.OP
{
    public class IPController : ControllerBase, IPlotController
    {
        public IPController()
        {
            this.BindMouseEnter(PlotCommands.HoverPointsOnlyTrack);
        }
    }
}
