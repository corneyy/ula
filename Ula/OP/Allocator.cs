﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ula.OP
{
    public delegate void NewAlloc(IList<DataPoint> dp);

    public interface IAllocator
    {
        void Alloc(NewAlloc eh);
    }

    public class ListAllocator : IAllocator
    {
        public static readonly ListAllocator instance = new ListAllocator();

        ListAllocator() { }

        public void Alloc(NewAlloc eh)
        {
            eh(new List<DataPoint>());
        }
    }

    public abstract class ReAllocator: IAllocator
    {
        List<NewAlloc> ehs=new List<NewAlloc>();

        public void Alloc(NewAlloc eh)
        {
            ehs.Add(eh);

            eh(Allocation());
        }

        protected void ReallocateAll()
        {
            ehs.ForEach( (eh)=> eh(Allocation()) );
        }

        protected abstract IList<DataPoint> Allocation();
    }
}
