﻿using System.Windows;
using System.Windows.Controls;
using Ula.Features.LogPlots;

namespace Ula.UI
{
    /// <summary>
    /// Interaction logic for LogPlots.xaml
    /// </summary>
    public partial class LogPlots : TabControl
    {
        DetailPlotsViewModel detailVM = new DetailPlotsViewModel();

        public LogPlots()
        {
            InitializeComponent();
        }
    }
}
