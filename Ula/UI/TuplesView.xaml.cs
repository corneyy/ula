﻿using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace Ula.UI
{
    /// <summary>
    /// Interaction logic for TuplesView.xaml
    /// </summary>
    public partial class TuplesView : Grid
    {
        public static DependencyProperty HeaderTextProperty = DependencyProperty.Register("HeaderText", typeof(string), typeof(TuplesView));
        public static DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(TuplesView));

        public TuplesView()
        {
            InitializeComponent();
        }

        [BindableAttribute(true)]
        public string HeaderText
        {
            get { return (string)GetValue(HeaderTextProperty); }
            set { SetValue(HeaderTextProperty, value); }
        }

        [BindableAttribute(true)]
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }
    }
}
