﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Ula.UI
{
    /// <summary>
    /// Interaction logic for Config.xaml
    /// </summary>
    public partial class ConfigDialog : Window, INotifyPropertyChanged, IDataErrorInfo 
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string path;

        public ConfigDialog(string path)
        {
            this.path = path;

            InitializeComponent();

            this.DataContext = this;
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SelectAdb(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = 
                new Microsoft.Win32.OpenFileDialog()
                {
                    FileName = "adb",
                    Filter="Exe Files (*.exe)|*.exe|All Files (*.*)|*.*"
                };

            if (dlg.ShowDialog() == true) AdbPath = dlg.FileName;
        }

        public string AdbPath
        {
            get { return path; }
            set { path = value; OnPropertyChanged("AdbPath"); }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string this[string columnName]
        {
            get
            {
                return (null != path && (0 == path.Length || System.IO.File.Exists(path)))?null:"Path incorrect";
            }
        }

        public string Error
        {
            get { return "error "; }
        }
    }
}
