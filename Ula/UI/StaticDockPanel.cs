﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace Ula.UI
{
    public class StaticDockPanel: DockPanel
    {
        public static readonly DependencyProperty UserFrozeProperty =
            DependencyProperty.Register(
                "UserFrozeCommand",
                typeof(ICommand),
                typeof(StaticDockPanel),
                null);

        public static readonly RoutedEvent UserFrozeEvent =
            EventManager.RegisterRoutedEvent(
                "UserFroze",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(StaticDockPanel));

        DispatcherTimer userFrozeTimer;

        static StaticDockPanel()
        {
            
        }

        public ICommand UserFrozeCommand
        {
            get { return (ICommand)GetValue(UserFrozeProperty); }
            set { SetValue(UserFrozeProperty, value); }
        }

        public event RoutedEventHandler UserFroze
        {
            add { AddHandler(UserFrozeEvent, value); }
            remove { RemoveHandler(UserFrozeEvent, value); }
        }

        void RaiseUserFrozeEvent()
        {
            RaiseEvent(new RoutedEventArgs(UserFrozeEvent));
        }

        protected override void OnInitialized(EventArgs e)
        {
            userFrozeTimer = new DispatcherTimer();
            userFrozeTimer.Interval = TimeSpan.FromSeconds(1);
            userFrozeTimer.Tick += delegate
            {
                userFrozeTimer.Stop();
                OnUserFroze();
            };
            
            base.OnInitialized(e);
        }

        protected void OnUserFroze()
        {
            RaiseUserFrozeEvent();
            if (UserFrozeCommand != null && UserFrozeCommand.CanExecute(null)) UserFrozeCommand.Execute(null);
        }

        protected override void OnMouseEnter( System.Windows.Input.MouseEventArgs e)
        {
            userFrozeTimer.Start();

            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
        {
            userFrozeTimer.Stop();

            base.OnMouseLeave(e);
        }

        protected override void OnMouseLeftButtonDown(System.Windows.Input.MouseButtonEventArgs e)
        {
            userFrozeTimer.Stop();
            OnUserFroze();

            base.OnMouseLeftButtonDown(e);
        }
    }
}
