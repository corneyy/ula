﻿using OxyPlot;

namespace Ula.Plots
{
    public class StatSeries : IPlotIPSampleSeries
    {
        public delegate Unity.IPSample.Stat Extractor(Unity.IPSample sample);

        string title;
        OP.FixedStairStepSeries lineSeries;
        OP.FixedAreaSeries areaSeries;
        Extractor extractor;

        public StatSeries(string axisKey, string title, Extractor extractor,
            OxyColor color, Ula.OP.IAllocator allocator, string trackerFormat = "{0}\n{4:0.#} ms")
        {
            this.title = title;
            this.extractor = extractor;

            lineSeries = new OP.FixedStairStepSeries(allocator)
            {
                YAxisKey = axisKey,
                Title = title + " avg",
                TrackerFormatString = trackerFormat,
                StrokeThickness = 1,
                CanTrackerInterpolatePoints = false,
                Smooth = false,
                Color = color,
                VerticalStrokeThickness = 0.5
            };

            areaSeries = new OP.FixedAreaSeries(allocator)
            {
                YAxisKey = axisKey,
                Title = title + " min/max",
                TrackerFormatString = trackerFormat,
                CanTrackerInterpolatePoints = false,
                Smooth = false,
                StrokeThickness = 0,
                Fill = OxyColor.FromAColor(50, color),
            };
        }

        public virtual void AddTo(PlotModel model)
        {
            model.Series.Add(areaSeries);
            model.Series.Add(lineSeries);
        }

        public void Add(int idx, double min, double max, double avg)
        {
            areaSeries.Points.Add(new DataPoint(idx, min));
            areaSeries.Points2.Add(new DataPoint(idx, max));
            lineSeries.Points.Add(new DataPoint(idx, avg));
        }

        public void Add(int idx, Unity.IPSample.Stat stat)
        {
            Add(idx, stat.min, stat.max, stat.avg);
        }

        public virtual void Add(int idx, Unity.IPSample sample)
        {
            Add(idx, extractor(sample));
        }

        public virtual void Clear()
        {
            areaSeries.Points.Clear();
            areaSeries.Points2.Clear();
            lineSeries.Points.Clear();
        }
    }

    class StatBatchedSeries : StatSeries
    {
        public new delegate Unity.IPSample.StatBatched Extractor(Unity.IPSample sample);

        const string trackerFormat = "{0}\n{4}";
        Extractor extractor;
        OP.FixedLineSeries batchedSeries;

        public StatBatchedSeries(string axisKey, string title, Extractor extractor, OxyColor color, Ula.OP.IAllocator allocator)
            : base(axisKey, title, null, color, allocator, trackerFormat)
        {
            this.extractor = extractor;

            batchedSeries = new OP.FixedLineSeries(allocator)
            {
                YAxisKey = axisKey,
                Title = title + " batched",
                TrackerFormatString = trackerFormat,
                StrokeThickness = 2,
                LineStyle = LineStyle.Dash,
                CanTrackerInterpolatePoints = false,
                Smooth = false,
                Color = color,
            };
        }

        public override void AddTo(PlotModel model)
        {
            base.AddTo(model);

            model.Series.Add(batchedSeries);
        }

        public new void Add(int idx, Unity.IPSample.Stat stat)
        {
            throw new System.NotImplementedException();
        }

        public void Add(int idx, Unity.IPSample.StatBatched stat)
        {
            Add(idx, stat.min, stat.max, stat.avg);
            batchedSeries.Points.Add(new DataPoint(idx, stat.batched));
        }

        public override void Add(int idx, Unity.IPSample sample)
        {
            Add(idx, extractor(sample));
        }

        public override void Clear()
        {
            base.Clear();

            batchedSeries.Points.Clear();
        }
    }
}
