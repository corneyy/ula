﻿using OxyPlot;

namespace Ula.Plots
{
    public class RangeSeries : IPlotIPSampleSeries
    {
        public struct Range
        {
            public double b;
            public double e;
            public Range(double b, double e)
            {
                this.b = b;
                this.e = e;
            }
        }

        public delegate Range Extractor(Unity.IPSample sample);

        string title;
        OP.FixedAreaSeries areaSeries;
        Extractor extractor;

        public RangeSeries(string axisKey, string title, Extractor extractor,
            OxyColor color, Ula.OP.IAllocator allocator, string trackerFormat = "{0}\n{4}")
        {
            this.title = title;
            this.extractor = extractor;

            areaSeries = new OP.FixedAreaSeries(allocator)
            {
                YAxisKey = axisKey,
                Title = title + " min/max",
                TrackerFormatString = trackerFormat,
                CanTrackerInterpolatePoints = false,
                Smooth = false,
                StrokeThickness = 0,
                Fill = OxyColor.FromAColor(100, color),
            };
        }

        public void AddTo(PlotModel model)
        {
            model.Series.Add(areaSeries);
        }

        public void Add(int idx, Range r)
        {
            areaSeries.Points.Add(new DataPoint(idx, r.b));
            areaSeries.Points2.Add(new DataPoint(idx, r.e));
        }

        public void Add(int idx, Unity.IPSample sample)
        {
            Add(idx, extractor(sample));
        }

        public void Clear()
        {
            areaSeries.Points.Clear();
            areaSeries.Points2.Clear();
        }
    }
}
