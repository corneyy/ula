﻿
namespace Ula.Plots
{
    public interface IPlotSeries
    {
        void AddTo(OxyPlot.PlotModel model);
        void Clear();
    }

    public interface IPlotIPSampleSeries : IPlotSeries
    {
        void Add(int idx, Unity.IPSample sample);
    }

    public interface IPlotValueSeries : IPlotSeries
    {
        void Add(int idx, double value);
    }
}
