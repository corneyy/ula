﻿using OxyPlot;

namespace Ula.Plots
{
    public class SingleSeries : IPlotSeries
    {
        string title;
        OP.FixedStairStepSeries lineSeries;

        public SingleSeries(string axisKey, string title,
            OxyColor color, Ula.OP.IAllocator allocator,
            string trackerFormat = "{0}\n{4:0.#}", LineStyle lineStyle = LineStyle.Solid )
        {
            this.title = title;

            lineSeries = new OP.FixedStairStepSeries(allocator)
            {
                YAxisKey = axisKey,
                Title = title,
                TrackerFormatString = trackerFormat,
                StrokeThickness = 1,
                CanTrackerInterpolatePoints = false,
                Smooth = false,
                Color = color,
                LineStyle= lineStyle,
                VerticalLineStyle = lineStyle,
                VerticalStrokeThickness=0.5,
            };
        }

        #region IPlotSeries

        public void AddTo(PlotModel model)
        {
            model.Series.Add(lineSeries);
        }

        public void Clear()
        {
            lineSeries.Points.Clear();
        }
        #endregion

        public void Add(int idx, double value)
        {
            lineSeries.Points.Add(new DataPoint(idx, value));
        }
    }

    public class ValueSeries : SingleSeries, IPlotValueSeries
    {
        public ValueSeries(string axisKey, string title, OxyColor color, 
            Ula.OP.IAllocator allocator, LineStyle lineStyle = LineStyle.Solid,  string trackerFormat = "{0}\n{4}")
            : base(axisKey, title, color, allocator, trackerFormat, lineStyle) { }
    }

    public class ParameterSeries : SingleSeries, IPlotIPSampleSeries
    {
        public delegate double Extractor(Unity.IPSample sample);

        Extractor extractor;

        public ParameterSeries(string axisKey, string title, Extractor extractor,
            OxyColor color, Ula.OP.IAllocator allocator, string trackerFormat = "{0}\n{4:0.#}")
            :base(axisKey, title, color, allocator, trackerFormat)
        {
            this.extractor = extractor;
        }

        public void Add(int idx, Unity.IPSample sample)
        {
            Add(idx, extractor(sample));
        }
    }
}
