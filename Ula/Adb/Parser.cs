﻿using System;
using System.Linq;


namespace Ula.Adb
{
    public class LogRecord
    {
        public DateTime datetime;
        public string content;

        public LogRecord(string content)
        {
            datetime = new DateTime();
            this.content = content;
        }

        public LogRecord(DateTime dt, string content)
        {
            datetime = dt;
            this.content = content;
        }
    }


    public class Parser
    {
        public delegate void RecordReady(LogRecord record);

        static string checkStr = "Unity";
        //static string checkStr = "dalvikvm";
        //static string checkStr = "ActivityManager";
        static IFormatParser[] formatParsers = 
        {
            new BriefFormatParser(),
            new TimeFormatParser(),
            new TrueFormatParser()
        };

        public static void ParseOneToOne(string record, RecordReady readyCB)
        {
            if (record.Length < checkStr.Length + 3) return;
            LogRecord parsed = GetFormatParser(record).ParseRecord(record, checkStr);
            if (null != parsed.content) readyCB(parsed);
        }

        public static void Parse(Func<string> nextLine, RecordReady readyCB)
        {
            string record = nextLine();
            while (null != record)
            {
                if (record.Length > checkStr.Length + 3)
                {
                    ParseReturn ret = GetFormatParser(record).Parse(record, nextLine, readyCB, checkStr);
                    if (ret.isPeeked) record=ret.line;
                    else record = nextLine();
                }
                else
                {
                    record = nextLine();
                }
            }
        }

        static IFormatParser GetFormatParser(string record)
        {
            return formatParsers.First(p => p.IsValidFormat(record));
        }
    }

    internal struct ParseReturn
    {
        public bool isPeeked;
        public string line;
        public ParseReturn(string line)
        {
            isPeeked = true;
            this.line = line;
        }
    }

    internal interface IFormatParser
    {
        bool IsValidFormat(string record);

        ParseReturn Parse(string record, Func<string> nextLine, Parser.RecordReady readyCB, string checkStr);

        LogRecord ParseRecord(string record, string checkStr);
    }

    internal class TrueFormatParser : IFormatParser
    {
        public bool IsValidFormat(string record)
        {
            return true;
        }

        public ParseReturn Parse(string record, Func<string> nextLine, Parser.RecordReady readyCB, string checkStr)
        {
            return new ParseReturn();
        }

        public LogRecord ParseRecord(string record, string checkStr)
        {
            return new LogRecord(null);
        }
    }

    internal abstract class EmptySeparatedFormatParser: IFormatParser{
        protected readonly int maxAndroidLoggerPayloadSize = 1012;  
#region IFormatParser
        public bool IsValidFormat(string record)
        {
            return record.Length > MarkPos() && record[MarkPos()] == '/';
        }

        public ParseReturn Parse(string record, Func<string> nextLine, Parser.RecordReady readyCB, string checkStr)
        {
            LogRecord parsed = ParseRecord(record, checkStr);
            if (null == parsed.content) return new ParseReturn();

            readyCB(parsed);

            string nextRecord = NextRecord(nextLine);
            if (null != nextRecord)
            {
                LogRecord nextParsed = ParseRecord(nextRecord, checkStr);
                if (null != nextParsed.content && nextParsed.content.StartsWith("UnityEngine.Debug:"))
                {
                    nextRecord = SkipTrace(nextLine, checkStr, parsed.content.Length + nextParsed.content.Length);
                    if (null == nextRecord) return new ParseReturn();
                }
            }
            
            return new ParseReturn(nextRecord);
        }
#endregion

        protected string NextRecord(Func<string> nextLine)
        {
            string record = nextLine();
            while (null != record && 0 == record.Length) record = nextLine();
            return record;
        }

        protected bool IsPayloadLengthExceeded(int length)
        {
            return length >= maxAndroidLoggerPayloadSize;
        } 

#region Childs iface
        public abstract LogRecord ParseRecord(string record, string checkStr);

        protected abstract int MarkPos();

        protected abstract int PayloadPos(string checkStr);

        protected abstract string SkipTrace(Func<string> nextLine, string checkStr, int length);
#endregion
    }

    internal class BriefFormatParser : EmptySeparatedFormatParser
    {
        protected override int MarkPos()
        {
            return 1;
        }

        protected override int PayloadPos(string checkStr)
        {
            return checkStr.Length < 9 ? 19 : checkStr.Length + 10; // semi-fixed length format
        }

        protected override string SkipTrace(Func<string> nextLine, string checkStr, int length)
        {
            LogRecord parsed = NextParsed(nextLine, checkStr);
            while (null != parsed.content && 0 != parsed.content.Length && !IsPayloadLengthExceeded(length += parsed.content.Length))
            {
                parsed = NextParsed(nextLine, checkStr);
            }
            return null;
        }

        LogRecord NextParsed(Func<string> nextLine, string checkStr)
        {
            string record = NextRecord(nextLine);
            if (null == record) return new LogRecord(null);
            return ParseRecord(record, checkStr);
        }

        public override LogRecord ParseRecord(string record, string checkStr)
        {
            int payloadPos = PayloadPos(checkStr);
            if (record.Length < payloadPos || 0 != string.Compare(record, MarkPos() + 1, checkStr, 0, checkStr.Length)) return new LogRecord(null);
            return new LogRecord(record.Substring(payloadPos));
        }
    }

    internal class TimeFormatParser : EmptySeparatedFormatParser
    {
        struct Record
        {
            public string source;
            public string datetime;
            public string payload;
        }

        protected override int MarkPos()
        {
            return 20;
        }

        protected override int PayloadPos(string checkStr)
        {
            return checkStr.Length < 9 ? 38 : checkStr.Length + 30; // semi-fixed length format
        }

        protected override string SkipTrace(Func<string> nextLine, string checkStr, int length)
        {
            Record parsed = NextParsed(nextLine, checkStr);
            while (null != parsed.payload && 0 != parsed.payload.Length && !IsPayloadLengthExceeded(length += parsed.payload.Length))
            {
                string dt = parsed.datetime;
                parsed = NextParsed(nextLine, checkStr);
                if (dt != parsed.datetime) return parsed.source;
            }
            return null;
        }

        Record NextParsed(Func<string> nextLine, string checkStr)
        {
            string record = NextRecord(nextLine);
            if (null == record) return new Record();

            LogRecord payload = ParseRecord(record, checkStr);
            if(null==payload.content) return new Record();
            return new Record() { source=record, payload = payload.content, datetime=record.Substring(0,18)};
        }

        const string dtFormat = "MM-dd HH:mm:ss.fff";
        static readonly System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.InvariantCulture; 

        public override LogRecord ParseRecord(string record, string checkStr)
        {
            int payloadPos = PayloadPos(checkStr);
            if (record.Length < payloadPos || 0 != string.Compare(record, MarkPos() + 1, checkStr, 0, checkStr.Length)) return new LogRecord(null);
            
            DateTime dt=new DateTime();
            DateTime.TryParseExact(record.Substring(0, 18), dtFormat, ci, System.Globalization.DateTimeStyles.None, out dt);

            return new LogRecord(dt, record.Substring(payloadPos));
        }
    }
}
